// My Shoot Game C++

using UnrealBuildTool;
using System.Collections.Generic;

public class MyShooterCPPGameEditorTarget : TargetRules
{
	public MyShooterCPPGameEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "MyShooterCPPGame" } );
	}
}
