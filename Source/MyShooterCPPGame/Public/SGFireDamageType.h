// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "SGFireDamageType.generated.h"

/**
 * 
 */
UCLASS()
class MYSHOOTERCPPGAME_API USGFireDamageType : public UDamageType
{
	GENERATED_BODY()
	
};
