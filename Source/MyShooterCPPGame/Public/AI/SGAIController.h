// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "SGAIController.generated.h"

class USGAIPerceptionComponent;
class USGRespawnComponent;

UCLASS()
class MYSHOOTERCPPGAME_API ASGAIController : public AAIController
{
	GENERATED_BODY()
	
public:
	ASGAIController();

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "AIPerception")
		USGAIPerceptionComponent* SGAIPerception;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "AIPerception")
		USGRespawnComponent* SGRespawnComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
		FName FocusOnKeyName = "EnemyActor";

	virtual void OnPossess(APawn* InPawn) override;

	virtual void Tick(float DeltaTime) override;

private:

	AActor* GetFocusOnActor() const;
};
