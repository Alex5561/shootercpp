// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SGRespawnComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MYSHOOTERCPPGAME_API USGRespawnComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	USGRespawnComponent();

	void Respawn(int32 RespawnTime);

	int32 GetRespawnTime() const { return RespawnCountDown; }
	bool IsRespawnInProgress() const;

private:

	FTimerHandle RespawnTimerHandle;

	int32 RespawnCountDown = 0;

	void RespawnTimerUpdate();
};
