// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "SGChangeWeaponServiseService.generated.h"

/**
 * 
 */
UCLASS()
class MYSHOOTERCPPGAME_API USGChangeWeaponServiseService : public UBTService
{
	GENERATED_BODY()
	
public:
	USGChangeWeaponServiseService();

protected:

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Probablity", meta = (ClampMin = "0.0", ClampMax = "1.0"))
		float Probabality = 0.5f;

};
