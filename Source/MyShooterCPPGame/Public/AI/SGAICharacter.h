// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "Player/SGBaseCharacter.h"
#include "SGAICharacter.generated.h"

class UBehaviorTree;
class UWidgetComponent;


UCLASS()
class MYSHOOTERCPPGAME_API ASGAICharacter : public ASGBaseCharacter
{
	GENERATED_BODY()

public:

	ASGAICharacter(const FObjectInitializer& ObjectInit);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "BT")
		UBehaviorTree* BehaviorTreeAsset;

	virtual void OnDeath() override;

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UWidgetComponent* HealthBarWidget;

	virtual void OnhealthChange(float HealthValue, float HealthDelta) override;
	virtual void BeginPlay() override;
};
