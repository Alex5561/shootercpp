// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "SGFindEnemyServiceService.generated.h"

/**
 * 
 */
UCLASS()
class MYSHOOTERCPPGAME_API USGFindEnemyServiceService : public UBTService
{
	GENERATED_BODY()
public:
	USGFindEnemyServiceService();
	
protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	FBlackboardKeySelector EnemyActorKey;

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
