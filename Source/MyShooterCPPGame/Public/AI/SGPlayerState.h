// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "SGPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class MYSHOOTERCPPGAME_API ASGPlayerState : public APlayerState
{
	GENERATED_BODY()

public:
	void SetTeamID(int32 ID) { TeamID = ID; }
	int32 GetTeamID() const { return TeamID; }

	void SetTeamColor(const FLinearColor& Color) { TeamColor = Color; }
	FLinearColor GetTeamColor() const { return TeamColor; }

	void AddKill() { ++KillsNum; }
	int32 GetKillsNum()const { return KillsNum; }
	void AddDeath() { ++DeadsNum; }
	int32 GetDeadthNum()const { return DeadsNum; }


private:

	int32 TeamID;
	FLinearColor TeamColor;

	int32 KillsNum = 0;
	int32 DeadsNum = 0;
};
