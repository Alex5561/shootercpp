// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "EnvironmentQuery/EnvQueryTest.h"
#include "EnvQueryTest_PickUPCallBeTaken.generated.h"

/**
 * 
 */
UCLASS()
class MYSHOOTERCPPGAME_API UEnvQueryTest_PickUPCallBeTaken : public UEnvQueryTest
{
	GENERATED_BODY()
	
public:
	UEnvQueryTest_PickUPCallBeTaken(const FObjectInitializer& ObjInit);
	virtual void RunTest(FEnvQueryInstance& QueryInstance) const override;
};
