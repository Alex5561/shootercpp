// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "SGFireService.generated.h"


UCLASS()
class MYSHOOTERCPPGAME_API USGFireService : public UBTService
{
	GENERATED_BODY()

public:
	USGFireService();

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
		FBlackboardKeySelector EnemyActorKey;

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

};
