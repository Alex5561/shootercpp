// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "SGAmmoDecoratorDecorator.generated.h"

class ASGBaseWeapon;

UCLASS()
class MYSHOOTERCPPGAME_API USGAmmoDecoratorDecorator : public UBTDecorator
{
	GENERATED_BODY()
	
public:
	USGAmmoDecoratorDecorator();

protected:

	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		TSubclassOf<ASGBaseWeapon> WeaponType;

};
