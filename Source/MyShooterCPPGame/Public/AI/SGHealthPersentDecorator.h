// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "SGHealthPersentDecorator.generated.h"

/**
 * 
 */
UCLASS()
class MYSHOOTERCPPGAME_API USGHealthPersentDecorator : public UBTDecorator
{
	GENERATED_BODY()
public:
	USGHealthPersentDecorator();

protected:
	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float HealthPersent = 0.6f;
};
