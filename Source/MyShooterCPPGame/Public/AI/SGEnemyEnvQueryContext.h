// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "EnvironmentQuery/EnvQueryContext.h"
#include "SGEnemyEnvQueryContext.generated.h"

/**
 * 
 */
UCLASS()
class MYSHOOTERCPPGAME_API USGEnemyEnvQueryContext : public UEnvQueryContext
{
	GENERATED_BODY()
	
public:
		virtual void ProvideContext(FEnvQueryInstance& QueryInstance, FEnvQueryContextData& ContextData) const override;

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	FName EnemyActorKeyName =  "EnemyActor";
};
