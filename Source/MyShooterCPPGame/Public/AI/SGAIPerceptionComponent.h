// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "Perception/AIPerceptionComponent.h"
#include "SGAIPerceptionComponent.generated.h"

/**
 * 
 */
UCLASS()
class MYSHOOTERCPPGAME_API USGAIPerceptionComponent : public UAIPerceptionComponent
{
	GENERATED_BODY()
	
public:
	AActor* GetClossesEnemy() const;
};
