// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SGCore.h"
#include "SGGameModeBase.generated.h"



class ASGAIController;
UCLASS()
class MYSHOOTERCPPGAME_API ASGGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASGGameModeBase();

	FOnMatchStateChange OnMatchStateChange;

	virtual void StartPlay() override;
	virtual UClass* GetDefaultPawnClassForController_Implementation(AController* InController) override;
	void Killed(AController* KillerController, AController* VikcingController);

	FGameData GetData() const { return GameData; }
	int32 GetGameRound() const { return CurrentRound; }
	int32 GetRoundSecond() const { return RoundCountDownn; }

	void RespawnRequest(AController* Controller);

	virtual bool SetPause(APlayerController* PC, FCanUnpause CanUnpauseDelegate = FCanUnpause()) override;
	virtual bool ClearPause() override;
protected:

	UPROPERTY(EditDefaultsOnly, Category = "Game")
	TSubclassOf<ASGAIController> AIControllerClass;

	UPROPERTY(EditDefaultsOnly, Category = "Game")
	TSubclassOf<APawn> AIPawnClass;

	UPROPERTY(EditDefaultsOnly, Category = "Game")
	FGameData GameData;

	

private:
	
	ESGMathState MatchState = ESGMathState::WaitingToStart;
	int32 CurrentRound = 1;
	int32 RoundCountDownn = 0;
	FTimerHandle GameRoundTimerHandle;
	
	void SpawnBots();
	void StartRound();
	void GameTimerUpdate();

	void ResetPlayers();
	void ResetOnePlayer(AController* Controller);

	void CreateTeamsInfo();
	FLinearColor DetermineColorByTeamID(int32 TeamID) const;
	void SetPlayerColor(AController* Controller);

	void StartRespawn(AController* Controller);
	void GameOver();

	void SetMatchState(ESGMathState State);

	void StopAllFire();
};
