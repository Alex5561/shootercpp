// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "SGMenuPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class MYSHOOTERCPPGAME_API ASGMenuPlayerController : public APlayerController
{
	GENERATED_BODY()
	
protected:

	virtual void BeginPlay() override;
};
