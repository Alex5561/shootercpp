// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "SGMenuHUD.generated.h"

/**
 * 
 */
UCLASS()
class MYSHOOTERCPPGAME_API ASGMenuHUD : public AHUD
{
	GENERATED_BODY()
	
protected:

	virtual void BeginPlay() override;


	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "MenuWidget")
		TSubclassOf<UUserWidget> MenuWidgetclass;
};
