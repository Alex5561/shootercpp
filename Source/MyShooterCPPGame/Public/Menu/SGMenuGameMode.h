// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SGMenuGameMode.generated.h"

/**
 * 
 */
UCLASS()
class MYSHOOTERCPPGAME_API ASGMenuGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	ASGMenuGameMode();
};
