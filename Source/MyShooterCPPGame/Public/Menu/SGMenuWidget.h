// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SGCore.h"
#include "SGMenuWidget.generated.h"

class UButton;
class UHorizontalBox;
class USGGameInstance;
class USGLevelItemWidget;
class USoundCue;

UCLASS()
class MYSHOOTERCPPGAME_API USGMenuWidget : public UUserWidget
{
	GENERATED_BODY()
	
protected:

	virtual void NativeOnInitialized() override;

	UPROPERTY(meta = (BindWidget))
		UButton* StartGameButtonclass;

	UPROPERTY(meta = (BindWidget))
		UButton* QuitGameButtonclass;

	UPROPERTY(meta = (BindWidget))
		UHorizontalBox* LevelBoxclass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WidgetItem")
		TSubclassOf<UUserWidget> ItemLevelWidget;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
		USoundCue* StartGameSound;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
		USoundCue* OpenGameSound;

	UPROPERTY(Transient, meta = (BindWidgetAnim))
		UWidgetAnimation* HideAnimation;


private:
	UPROPERTY()
		TArray<USGLevelItemWidget*> LevelItemWidgets;

	virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation) override;

	UFUNCTION()
	void OnStartGame();
	UFUNCTION()
	void OnQuitGame();

	void InitLevelItem();
	void OnLevelSelected(const FLevelData& Data);
	USGGameInstance* GetGameInstanceCurrent();
};
