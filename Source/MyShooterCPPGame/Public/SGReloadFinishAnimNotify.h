// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "SGAnimNotify.h"
#include "SGReloadFinishAnimNotify.generated.h"

/**
 * 
 */
UCLASS()
class MYSHOOTERCPPGAME_API USGReloadFinishAnimNotify : public USGAnimNotify
{
	GENERATED_BODY()
	
};
