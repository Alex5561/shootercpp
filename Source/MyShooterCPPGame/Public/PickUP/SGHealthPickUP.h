// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "PickUP/SGBasePickUP.h"
#include "SGHealthPickUP.generated.h"

/**
 * 
 */
UCLASS()
class MYSHOOTERCPPGAME_API ASGHealthPickUP : public ASGBasePickUP
{
	GENERATED_BODY()

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ValueHealth", meta = (ClampMin ="1.0", ClampMax = "100.0"))
		float ValueChangeHealth = 35.f;
	
private:

	virtual bool GetPickUPTo(APawn* PlayerPawn) override;
};
