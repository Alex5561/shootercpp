// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SGBasePickUP.generated.h"

class USphereComponent;
class USoundCue;

UCLASS()
class MYSHOOTERCPPGAME_API ASGBasePickUP : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASGBasePickUP();

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CollisionPickUP")
		USphereComponent* CollisionComponent;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CollisionPickUP")
		float RespawnTime = 15.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SoundPickUP")
		USoundCue* SoundPickUP;

	virtual void BeginPlay() override;

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	bool ColdBeTaken() const;
private:

	FTimerHandle RespawnTimerHandle;

	virtual bool GetPickUPTo(APawn* PlayerPawn);

	void PickUPTaken();
	void Respawn();
};
