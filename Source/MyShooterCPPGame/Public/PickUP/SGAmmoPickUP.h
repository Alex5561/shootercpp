// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "PickUP/SGBasePickUP.h"
#include "SGAmmoPickUP.generated.h"

class ASGBaseWeapon;

UCLASS()
class MYSHOOTERCPPGAME_API ASGAmmoPickUP : public ASGBasePickUP
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PickUP", meta = (ClampMin = "1.0", ClampMax = "10.0"))
		int32 ClipsAmount = 10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PickUP")
		TSubclassOf<ASGBaseWeapon> WeaponType;


	
private:

	virtual bool GetPickUPTo(APawn* PlayerPawn) override;

};
