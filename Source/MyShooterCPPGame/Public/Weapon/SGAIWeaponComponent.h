// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "Weapon/SGWeaponComponent.h"
#include "SGAIWeaponComponent.generated.h"



UCLASS()
class MYSHOOTERCPPGAME_API USGAIWeaponComponent : public USGWeaponComponent
{
	GENERATED_BODY()
	
public:

	virtual void StartFire() override;
	virtual void NextWeapon() override;

};
