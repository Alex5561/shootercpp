// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SGCore.h"
#include "SGWeaponComponent.generated.h"

class ASGBaseWeapon;



UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MYSHOOTERCPPGAME_API USGWeaponComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	USGWeaponComponent();

protected:
	
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	TArray<FWeaponData> WeaponData;
	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	FName WeaponEquipSocketName = "WeaponSocket";
	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	FName WeaponArmorySocketName = "ArmorySocket";
	UPROPERTY(EditDefaultsOnly, Category = "Animation")
	UAnimMontage* EquipMontage;

public:	
	UPROPERTY()
	ASGBaseWeapon* CurrentWeapon = nullptr;

	UPROPERTY()
	TArray<ASGBaseWeapon*> Weapons;

	UPROPERTY()
	UAnimMontage* CurrentReloadAnimMOntage = nullptr;
	
	virtual void StartFire();
	void StopFire();
	virtual void NextWeapon();
	void Reload();

	bool GetDataUIWeapon(FWeaponUIData& UIData) const;
	bool GetWeaponAmmoData(FAmmoData& AmmoData) const;

	bool CanFire();
	bool CanEquip();

	bool TryToAddAmmo(TSubclassOf<ASGBaseWeapon> WeaponType, int32 ClipsAmount);

	void EquipWeapon(int32 WeaponIndex);
	int32 CurrentWeaponIndex = 0;

	bool NeedAmmo(TSubclassOf<ASGBaseWeapon> WeaponType);
private:

	void SpawnWeapons();

	
	bool EquipAnimToProgress = false;
	bool ReloadAnimToProgress = false;
	
	void AttachWeaponToSocket(ASGBaseWeapon* Weapon, USceneComponent* Component, const FName& Socket);
	
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
	void PlayAnimMontage(UAnimMontage* Montage);
	void InitAnimations();
	void OnEquipFinished(USkeletalMeshComponent* MeshComp);
	void OnReloadFinished(USkeletalMeshComponent* MeshComp);

	
	bool CanReload();

	void OnEmptyClip(ASGBaseWeapon* AmmoEmptyWeapon);
	void ChangeClip();

	template<typename T>
	T* FindNotifyByClass(UAnimSequenceBase* Animation)
	{
		if (!Animation) return nullptr;
		const auto NotifyEvents = Animation->Notifies;

		for (auto Notyfy : NotifyEvents)
		{
			auto AnimNotify = Cast<T>(Notyfy.Notify);
			if (AnimNotify)
			{
				return AnimNotify;
			}
		}
		return nullptr;
	}

};
