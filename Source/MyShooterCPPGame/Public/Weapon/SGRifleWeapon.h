// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "Weapon/SGBaseWeapon.h"
#include "SGRifleWeapon.generated.h"

class USGImpactWeaponFX;
class UNiagaraSystem;
class UNiagaraComponent;
class USoundCue;
class UAudioComponent;


UCLASS()
class MYSHOOTERCPPGAME_API ASGRifleWeapon : public ASGBaseWeapon
{
	GENERATED_BODY()
	
public:
	ASGRifleWeapon();

	virtual void StartFire() override;
	virtual void StopFire() override;

	

protected:

	virtual void MakeShot() override;
	virtual bool GetTraceData(FVector& StartTrace, FVector& EndTrace) override;

	UPROPERTY(VisibleAnywhere, Category = "VFX")
	USGImpactWeaponFX* FXComponent;

	UPROPERTY(EditDefaultsOnly, Category = "Damage")
		float RateShot = 0.3f;
	UPROPERTY(EditDefaultsOnly, Category = "Damage")
		float RandomShot = 1.5f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
		UNiagaraSystem* TraceFX;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
		FString TraceTargetName = "TraceTarget";
	UPROPERTY()
		UAudioComponent* FireAudioComponent;

	

	virtual void BeginPlay() override;

private:

	FTimerHandle ShotTimerHAndle;

	UPROPERTY()
	UNiagaraComponent* MuzzleFXComponent;

	void InitMuzzleFX();
	void SetMuzzleFXVisibility(bool Visibility);
	void SpawnTraceFX(const FVector& StartTrace, const FVector& EndTrace);


};
