// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "Weapon/SGBaseWeapon.h"
#include "SGLauncherWeapon.generated.h"

class ASGProjectileLauncher;
class USoundCue;

UCLASS()
class MYSHOOTERCPPGAME_API ASGLauncherWeapon : public ASGBaseWeapon
{
	GENERATED_BODY()
	
public:

	virtual void StartFire() override;

protected:
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Projectile")
		TSubclassOf<ASGProjectileLauncher> ProjectileClass;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Damage")
		USoundCue* NoAmmoSound;



	virtual void MakeShot() override;
};
