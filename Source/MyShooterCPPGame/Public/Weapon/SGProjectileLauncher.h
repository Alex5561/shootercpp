// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SGProjectileLauncher.generated.h"

class USphereComponent;
class UProjectileMovementComponent;
class USGImpactWeaponFX;

UCLASS()
class MYSHOOTERCPPGAME_API ASGProjectileLauncher : public AActor
{
	GENERATED_BODY()
	
public:	
	
	ASGProjectileLauncher();

protected:
	
	UPROPERTY(VisibleDefaultsOnly, Category = "Collision")
	USphereComponent* SphereCollision;
	UPROPERTY(VisibleDefaultsOnly, Category = "ProjectileMovement")
	UProjectileMovementComponent* ProjectileMovement;
	UPROPERTY(VisibleAnywhere, Category = "VFX")
	USGImpactWeaponFX* FXComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Damage")
		float RadiusDamage = 150.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Damage")
		float AmountDamage = 55.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Damage")
		bool DoFullDamage = false;

	virtual void BeginPlay() override;

public:

	void SetShotDirection(const FVector& Direction) { ShotDirection = Direction; }

private:

	FVector ShotDirection;

	UFUNCTION()
		void OnProjectileHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	AController* GetController();
};
