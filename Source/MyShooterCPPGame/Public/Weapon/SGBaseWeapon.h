// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SGCore.h"
#include "SGBaseWeapon.generated.h"



class USkeletalMeshComponent;
class UNiagaraSystem;
class UNiagaraComponent;
class USoundCue;

UCLASS()
class MYSHOOTERCPPGAME_API ASGBaseWeapon : public AActor
{
	GENERATED_BODY()
	
public:	

	ASGBaseWeapon();

	FOnClipEmpty OnClipEmpty;

	FWeaponUIData GetUIData() const { return UIData; }
	FAmmoData GetAmmoData() const { return CurrentAmmo;}

	bool IsAmmoEmpty() const;
	bool IsAmmoFool() const;

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		USkeletalMeshComponent* WeaponMesh;
	UPROPERTY(EditDefaultsOnly, Category = "Damage")
	float DamageValue = 3.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Damage")
		FAmmoData DefaultAmmo {19, 10, false};
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Damage")
		USoundCue* FireSound;

	FName MuzzleSocket = "MuzzleSocket";
	
	void DecreaseAmmo();
	
	bool IsClipEmpty() const;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UI")
		FWeaponUIData UIData;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UI")
		UNiagaraSystem* MuzzleFX;

	UNiagaraComponent* SpawnMuzzleFX();

	AController* GetController();

	

	virtual void BeginPlay() override;
public:
	virtual void StartFire();
	virtual void StopFire();
	virtual void MakeShot();
	void ChangeClip();
	bool CanReload() const;
	bool TrytoAddAmmo(int32 ClipsAmount);

	
	bool GetPlayerViemPoint(FVector& Location, FRotator& Rotation) ;
	FVector GetMuzzleWorldLocation() const;
	virtual bool GetTraceData(FVector& StartTrace, FVector& EndTrace);
	void MakeHit(FHitResult& HitResult, const FVector& TraceStart, const FVector& TraceEnd);
	void MakeDamage(FHitResult& ResultHit);

private:
	FAmmoData CurrentAmmo;
};
