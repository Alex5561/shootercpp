// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SGCore.h"
#include "SGImpactWeaponFX.generated.h"

class UNiagaraSystem;
class UPhysicalMaterial;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MYSHOOTERCPPGAME_API USGImpactWeaponFX : public UActorComponent
{
	GENERATED_BODY()

public:	
	
	USGImpactWeaponFX();

	void PlayImpactFX(const FHitResult Hit);

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
		FImpactData DefaultImpactData;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
		TMap<UPhysicalMaterial*, FImpactData> ImpactDataMap;



		
};
