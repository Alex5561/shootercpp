// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "SGCore.h"
#include "SGGameInstance.generated.h"


UCLASS()
class MYSHOOTERCPPGAME_API USGGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:

	FLevelData GetStartupLevel() { return StartupLevelName; }
	void SetLevelData(const FLevelData& Data) { StartupLevelName = Data; }
	FName GetMenuLevel() { return ManuLevelName; }
	TArray<FLevelData> GetLevelsData() const { return LevelsData; }

protected:

	UPROPERTY(EditDefaultsOnly, Category = "LevelStart")
		TArray<FLevelData> LevelsData;

	UPROPERTY(EditDefaultsOnly, Category = "NameLevelStart")
		FName ManuLevelName = NAME_None;

private:

	FLevelData StartupLevelName;

};
