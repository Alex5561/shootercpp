// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SGBackMainMenuWidget.generated.h"

class UButton;

UCLASS()
class MYSHOOTERCPPGAME_API USGBackMainMenuWidget : public UUserWidget
{
	GENERATED_BODY()
	
protected:

	virtual void NativeOnInitialized() override;

	UPROPERTY(meta = (BindWidget))
		UButton* BackMenuButton;

private:

	UFUNCTION()
	void OnChangeMainMenu();
};
