// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SGCore.h"
#include "SGGameOverWidget.generated.h"

class UVerticalBox;
class UButton;

UCLASS()
class MYSHOOTERCPPGAME_API USGGameOverWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:



	virtual void NativeOnInitialized() override;

	protected:

		UPROPERTY(meta = (BindWidget))
			UVerticalBox* PlayerStatBox;

		UPROPERTY(meta = (BindWidget))
			UButton* ResetLevelButton;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
			TSubclassOf<UUserWidget> StatsWidget;

private:


	void OnMatchStateChanged(ESGMathState State);
	void UpdatePlayerStat();

	UFUNCTION()
		void OnResetLevel();
};
