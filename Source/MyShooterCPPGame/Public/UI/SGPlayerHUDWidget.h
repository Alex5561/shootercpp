// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SGCore.h"
#include "SGPlayerHUDWidget.generated.h"



UCLASS()
class MYSHOOTERCPPGAME_API USGPlayerHUDWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable, Category = "UI")
	float GetPersentHealth() const;

	UFUNCTION(BlueprintCallable, Category = "UI")
	bool GetDataUIWeapon(FWeaponUIData& UIData) const;

	UFUNCTION(BlueprintCallable, Category = "UI")
	bool GetAmmoWeapon(FAmmoData& AmmoData) const;

	UFUNCTION(BlueprintCallable, Category = "UI")
		bool IsPlayerALife() const;

	UFUNCTION(BlueprintCallable, Category = "UI")
		bool IsPlayerSpactator() const;
	
	virtual bool Initialize() override;

	UFUNCTION(BlueprintImplementableEvent)
	void OnTakeDamage();

	UPROPERTY(meta = (BindWidgetAnim), Transient)
	UWidgetAnimation* DamageAnimation;

private:

	void OnHealthChange(float Health, float HealthDelta);
	void OnNewPawn(APawn* NewPawn);
};

