// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SGGameDataWidget.generated.h"

class ASGPlayerState;
class  ASGGameModeBase;

UCLASS()
class MYSHOOTERCPPGAME_API USGGameDataWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable)
	int32 GetKillsNum();
	UFUNCTION(BlueprintCallable)
	int32 GetCurrentRoundNum();
	UFUNCTION(BlueprintCallable)
	int32 GetTotalRoundsNum();
	UFUNCTION(BlueprintCallable)
	int32 GetRoundSecondRemaining();

private:
	ASGPlayerState* GetPlayerStateOw();
	ASGGameModeBase* GetGameModeOw();
};
