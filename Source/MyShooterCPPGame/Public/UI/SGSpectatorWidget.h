// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SGSpectatorWidget.generated.h"



UCLASS()
class MYSHOOTERCPPGAME_API USGSpectatorWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable, Category = "Respawn")
	bool GetRespawnTime(int32& CountDownTime);
};
