// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SGHealthBarWidget.generated.h"

class UProgressBar;

UCLASS()
class MYSHOOTERCPPGAME_API USGHealthBarWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

	void SetHealthPercentBar(float Value);

protected:

	UPROPERTY(meta = (BindWidget))
		UProgressBar* HealthBarClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
		float PersentVisibility = 0.8f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
		float PercentColor = 0.3f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
		FLinearColor GoodColor = FLinearColor::Green;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
		FLinearColor BadColor = FLinearColor::Red;
};
