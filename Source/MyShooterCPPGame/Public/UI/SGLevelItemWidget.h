// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SGCore.h"
#include "SGLevelItemWidget.generated.h"

class UButton;
class UImage;
class UTextBlock;



UCLASS()
class MYSHOOTERCPPGAME_API USGLevelItemWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	FOnLevelSelectedSignature OnLevelSelectedSignature;


	void SetLevelData(const FLevelData& Data);
	FLevelData GetLevelData() const { return LevelData; }
	void SetSelected(bool isSelected);

protected:

	UPROPERTY(meta = (BindWidget))
		UButton* LevelSelectionButton;

	UPROPERTY(meta = (BindWidget))
		UTextBlock* LevelNameTextBlock;

	UPROPERTY(meta = (BindWidget))
		UImage* LevelImage;

	UPROPERTY(meta = (BindWidget))
		UImage* FrameImage;

private:

	FLevelData LevelData;

	virtual void NativeOnInitialized() override;

	UFUNCTION()
		void OnLevelItemClick();

};
