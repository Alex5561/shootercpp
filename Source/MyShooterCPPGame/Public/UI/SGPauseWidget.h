// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SGPauseWidget.generated.h"

 class UButton;

UCLASS()
class MYSHOOTERCPPGAME_API USGPauseWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

	virtual bool Initialize() override;
	

protected:

	UPROPERTY(meta = (BindWidget))
		UButton* ClearPauseButtom;

private:

	UFUNCTION()
		void OnClearPause();
};
