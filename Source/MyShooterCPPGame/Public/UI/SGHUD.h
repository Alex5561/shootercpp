// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "SGCore.h"
#include "SGHUD.generated.h"


UCLASS()
class MYSHOOTERCPPGAME_API ASGHUD : public AHUD
{
	GENERATED_BODY()

public:
	virtual void DrawHUD() override;
	
protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "MainWidget")
		TSubclassOf<UUserWidget> PlayerHUDWidget;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "MainWidget")
		TSubclassOf<UUserWidget> PlayerPauseWidget;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "MainWidget")
		TSubclassOf<UUserWidget> GameOverWidget;

	virtual void BeginPlay() override;

private:

	UPROPERTY()
		TMap<ESGMathState, UUserWidget*> GameWidgets;

	UPROPERTY()
		UUserWidget* CurrentWidget = nullptr;

	void DrawCrossHair();
	void OnMatchStateChange(ESGMathState State);
	
};
