// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "SGIceDamageType.generated.h"

/**
 * 
 */
UCLASS()
class MYSHOOTERCPPGAME_API USGIceDamageType : public UDamageType
{
	GENERATED_BODY()
	
};
