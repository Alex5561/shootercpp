// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "SGCore.h"
#include "SGPlayerController.generated.h"


class USGRespawnComponent;

UCLASS()
class MYSHOOTERCPPGAME_API ASGPlayerController : public APlayerController
{
	GENERATED_BODY()

public:

	ASGPlayerController();

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "AIPerception")
		USGRespawnComponent* SGRespawnComponent;

	virtual void BeginPlay() override;

	virtual void OnPossess(APawn* InPawn) override;

	virtual void SetupInputComponent() override;

private:

	void OnPauseGame();
	void OnMatchStateChange(ESGMathState State);
};
