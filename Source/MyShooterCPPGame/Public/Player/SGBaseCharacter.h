// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "SGBaseCharacter.generated.h"

class UCameraComponent;
class USpringArmComponent;
class UHealthComponent;
class UTextRenderComponent;
class USGWeaponComponent;
class USphereComponent;


UCLASS()
class MYSHOOTERCPPGAME_API ASGBaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASGBaseCharacter(const FObjectInitializer& ObjectInit);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		UCameraComponent* CameraComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		USpringArmComponent* SpringArmComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		UHealthComponent* HealthComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		UTextRenderComponent* TextHealth;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		USGWeaponComponent* WeaponComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		USphereComponent* CameraCollisionComponent;

	UPROPERTY(EditDefaultsOnly, Category = "Landed")
		FVector2D LandedDamageVelocity = FVector2D(900.f, 1200.f);
	UPROPERTY(EditDefaultsOnly, Category = "Landed")
		FVector2D LandedDamage = FVector2D(10.f, 100.f);

	UFUNCTION()
		virtual void OnDeath();

	UFUNCTION()
		virtual void OnhealthChange(float HealthValue, float HealthDelta);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool isSprint = false;
	UPROPERTY(EditDefaultsOnly, Category = "AnimMontage")
		UAnimMontage* DeathMontage;

	UPROPERTY(EditDefaultsOnly, Category = "Material")
		FName MaterialColorName = "Paint Color";

	UFUNCTION(BlueprintCallable, Category = "CalculateDirection")
		float CalculateDirectionABP() const;

	void SetPlayerColor(const FLinearColor& Color);

	virtual void TurnOff() override;
	virtual void Reset() override;


private:

	void MoveForward(float Amount);
	void MoveRight(float Amount);

	void LookUP(float Amount);
	void Turn(float Amount);

	bool isForward = false;

	void OnSprint();
	void UnSprint();

	

	

	
	UFUNCTION()
	void OnGroundLanded(const FHitResult& HitResult);

	UFUNCTION()
	void OnCameraBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void OnCameraEndOverrlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	
	void CheckCameraOverlap();
};
