// My Shoot Game C++

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SGCore.h"
#include "HealthComponent.generated.h"

class UCameraShakeBase;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MYSHOOTERCPPGAME_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	
	UHealthComponent();

	FOnDead OnDead;
	FOnHealthChange OnHealthChange;


protected:
	
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "MaxHealth", meta = (Clampmin = "50" ,ClampMax = "150"))
	float MaxHealth = 100.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
		TSubclassOf<UCameraShakeBase> ShakeClass;



public:	

	float GetHealth() const { return Health; }
	float GetMaxHealth() const { return MaxHealth; }
	void ChangeHealth(float Value);

	UFUNCTION(BlueprintCallable)
		bool IsDead() const { return FMath::IsNearlyZero(Health); }

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Heal")
		bool AutoHeal = true;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Heal", meta = (EditCondition="AutoHeal"))
		float HealUpdateTime = 1.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Heal", meta = (EditCondition = "AutoHeal"))
		float HealDelay = 3.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Heal", meta = (EditCondition = "AutoHeal"))
		float HealModifaier = 5.0f;

	float GetHealthPercent() const { return Health / MaxHealth; }

private:

	float Health = 0.0f;

	FTimerHandle HealTimerHAndle;

	UFUNCTION()
		void OnTakeAnyDamageHandle(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);
	
	void HealUpdate();
	void SetHealth(float HealthVal);

	void PlayCameraShake();

	void Killed(AController* KillerController);

	void ReportDamage(float Damage, AController* InstigatorBy);
	
};
