// My Shoot Game C++


#include "Menu/SGMenuWidget.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "SGGameInstance.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Components/HorizontalBox.h"
#include "UI/SGLevelItemWidget.h"
#include "Sound/SoundCue.h"


void USGMenuWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (StartGameButtonclass)
	{
		StartGameButtonclass->OnClicked.AddDynamic(this, &USGMenuWidget::OnStartGame);
	}
	if (QuitGameButtonclass)
	{
		QuitGameButtonclass->OnClicked.AddDynamic(this, &USGMenuWidget::OnQuitGame);
	}
	InitLevelItem();
	UGameplayStatics::PlaySound2D(GetWorld(), OpenGameSound);
}

void USGMenuWidget::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
	if (Animation != HideAnimation) return;
	const auto GameInstanc = GetGameInstanceCurrent();
	if (!GameInstanc) return;

	UGameplayStatics::OpenLevel(this, GameInstanc->GetStartupLevel().LevelName);
}

void USGMenuWidget::OnStartGame()
{
	PlayAnimation(HideAnimation);
	UGameplayStatics::PlaySound2D(GetWorld(), StartGameSound);
}

void USGMenuWidget::OnQuitGame()
{
	UKismetSystemLibrary::QuitGame(this, GetOwningPlayer(), EQuitPreference::Quit, true);
}

void USGMenuWidget::InitLevelItem()
{
	const auto GameInstanc = GetGameInstanceCurrent();
	if(!GameInstanc) return;

	if (!LevelBoxclass) return;
	LevelBoxclass->ClearChildren();

	for (auto LevelData : GameInstanc->GetLevelsData())
	{
		const auto LevelItemWidget = CreateWidget<USGLevelItemWidget>(this, ItemLevelWidget);
		if (!LevelItemWidget) continue;

		LevelItemWidget->SetLevelData(LevelData);
		LevelItemWidget->OnLevelSelectedSignature.AddUObject(this, &USGMenuWidget::OnLevelSelected);

		LevelBoxclass->AddChild(LevelItemWidget);
		LevelItemWidgets.Add(LevelItemWidget);
	}

	if (GameInstanc->GetStartupLevel().LevelName.IsNone())
	{
		OnLevelSelected(GameInstanc->GetLevelsData()[0]);
	}
	else
	{
		OnLevelSelected(GameInstanc->GetStartupLevel());
	}
	
}

void USGMenuWidget::OnLevelSelected(const FLevelData& Data)
{
	const auto GameInstanc = GetGameInstanceCurrent();
	if (!GameInstanc) return;

	GameInstanc->SetLevelData(Data);

	for (auto LevelItemWidget : LevelItemWidgets)
	{
		if (LevelItemWidget)
		{
			const auto isSelected = Data.LevelName == LevelItemWidget->GetLevelData().LevelName;
			LevelItemWidget->SetSelected(isSelected);
		}
	}
}

USGGameInstance* USGMenuWidget::GetGameInstanceCurrent() 
{
	if (!GetWorld()) return nullptr;

	return GetWorld()->GetGameInstance<USGGameInstance>();
}
