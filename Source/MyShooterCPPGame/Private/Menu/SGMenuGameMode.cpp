// My Shoot Game C++


#include "Menu/SGMenuGameMode.h"
#include "Menu/SGMenuPlayerController.h"
#include "Menu/SGMenuHUD.h"

ASGMenuGameMode::ASGMenuGameMode()
{
	PlayerControllerClass = ASGMenuPlayerController::StaticClass();
	HUDClass = ASGMenuHUD::StaticClass();
}