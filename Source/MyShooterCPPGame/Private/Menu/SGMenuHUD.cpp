// My Shoot Game C++


#include "Menu/SGMenuHUD.h"
#include "Blueprint/UserWidget.h"

void ASGMenuHUD::BeginPlay()
{
	Super::BeginPlay();

	if (MenuWidgetclass)
	{
		const auto MenuWidget =CreateWidget(GetWorld(), MenuWidgetclass);
		if (MenuWidget)
		{
			MenuWidget->AddToViewport();
		}
	}
}