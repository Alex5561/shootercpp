// My Shoot Game C++


#include "SGAnimNotify.h"

void USGAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	OnNotifySignature.Broadcast(MeshComp);

	Super::Notify(MeshComp, Animation);
}