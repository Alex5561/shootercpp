// My Shoot Game C++


#include "AI/EnvQueryTest_PickUPCallBeTaken.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_ActorBase.h"
#include "PickUP/SGBasePickUP.h"


UEnvQueryTest_PickUPCallBeTaken::UEnvQueryTest_PickUPCallBeTaken(const FObjectInitializer& ObjInit) :Super(ObjInit)
{
	Cost = EEnvTestCost::Low;
	ValidItemType = UEnvQueryItemType_ActorBase::StaticClass();
	SetWorkOnFloatValues(false);
}

void UEnvQueryTest_PickUPCallBeTaken::RunTest(FEnvQueryInstance& QueryInstance) const
{
	for (FEnvQueryInstance::ItemIterator It(this, QueryInstance); It; ++It)
	{
		AActor* ItemActor = GetItemActor(QueryInstance, It.GetIndex());
		const auto PickUP = Cast<ASGBasePickUP>(ItemActor);
		if (!PickUP) continue;

		const auto ColdBeTaken = PickUP->ColdBeTaken();
		if (ColdBeTaken)
		{
			It.SetScore(TestPurpose, FilterType, true, true);
		}
		else
		{
			It.ForceItemState(EEnvItemStatus::Failed);
		}
	}
}
