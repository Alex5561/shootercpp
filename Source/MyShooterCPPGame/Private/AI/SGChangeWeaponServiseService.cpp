// My Shoot Game C++


#include "AI/SGChangeWeaponServiseService.h"
#include "Weapon/SGWeaponComponent.h"
#include "Weapon/SGAIWeaponComponent.h"
#include "AI/SGAIController.h"
#include "Util.h"

USGChangeWeaponServiseService::USGChangeWeaponServiseService()
{
	NodeName = "ChageWeapon";
}

void USGChangeWeaponServiseService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	const auto Controller = OwnerComp.GetAIOwner();
	if (Controller)
	{
		const auto WeaponComponent = Utils::GetComponent<USGWeaponComponent>(Controller->GetPawn());
		if (WeaponComponent && Probabality > 0 && FMath::FRand() <= Probabality)
		{
			WeaponComponent->NextWeapon();
		}
	}

	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}
