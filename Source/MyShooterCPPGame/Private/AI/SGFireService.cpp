// My Shoot Game C++


#include "AI/SGFireService.h"
#include "AI/SGAIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Weapon/SGWeaponComponent.h"
#include "Util.h"

USGFireService::USGFireService()
{
	NodeName = "FireServise";
}

void USGFireService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	const auto Controller = OwnerComp.GetAIOwner();
	const auto BlackBoard = OwnerComp.GetBlackboardComponent();

	const auto HasAim = BlackBoard && BlackBoard->GetValueAsObject(EnemyActorKey.SelectedKeyName);
	if (Controller)
	{
		const auto WeaponComponent = Utils::GetComponent<USGWeaponComponent>(Controller->GetPawn());
		if (WeaponComponent)
		{
			HasAim ? WeaponComponent->StartFire() : WeaponComponent->StopFire();
		}
	}

	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}
