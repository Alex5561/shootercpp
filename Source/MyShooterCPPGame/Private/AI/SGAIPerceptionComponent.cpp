// My Shoot Game C++


#include "AI/SGAIPerceptionComponent.h"
#include "AI/SGAIController.h"
#include "Util.h"
#include "Components/HealthComponent.h"
#include "Perception/AISense_Sight.h"
#include "Perception/AISense_Damage.h"

AActor* USGAIPerceptionComponent::GetClossesEnemy() const
{
	TArray<AActor*>PerciveActors;
	GetCurrentlyPerceivedActors(UAISense_Sight::StaticClass(), PerciveActors);
	if (PerciveActors.Num() == 0)
	{
		GetCurrentlyPerceivedActors(UAISense_Damage::StaticClass(), PerciveActors);
		if (PerciveActors.Num() == 0) return nullptr;
	}
	const auto Controller = Cast<AAIController>(GetOwner());
	if (!Controller) return nullptr;

	const auto Pawn = Controller->GetPawn();
	if (!Pawn) return nullptr;

	float BestDistance = MAX_FLT;
	AActor* BestPawn = nullptr;
	for (auto PerciveActor : PerciveActors)
	{
		const auto HealthComponent = Utils::GetComponent<UHealthComponent>(PerciveActor);

		const auto PercivePawn = Cast<APawn>(PerciveActor);
		const auto AreEnemies = PercivePawn && Utils::AreEnemis(Controller, PercivePawn->Controller);


		if (HealthComponent && !HealthComponent->IsDead() && AreEnemies)
		{
			const auto CurrentDistance = (PerciveActor->GetActorLocation() - Pawn->GetActorLocation()).Size();
			if (CurrentDistance < BestDistance)
			{
				BestDistance = CurrentDistance;
				BestPawn = PerciveActor;
			}
		}

	}
	return BestPawn;
}