// My Shoot Game C++


#include "AI/SGAIController.h"
#include "AI/SGAICharacter.h"
#include "AI/SGAIPerceptionComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AI/SGRespawnComponent.h"

ASGAIController::ASGAIController()
{
	SGAIPerception = CreateDefaultSubobject<USGAIPerceptionComponent>(TEXT("AIPerrceprion"));
	SetPerceptionComponent(*SGAIPerception);
	SGRespawnComponent = CreateDefaultSubobject<USGRespawnComponent>(TEXT("RespawnComponent"));
	
	bWantsPlayerState = true;
}

void ASGAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	const auto SGCharacter = Cast<ASGAICharacter>(InPawn);
	if (SGCharacter)
	{
		RunBehaviorTree(SGCharacter->BehaviorTreeAsset);
	}
}

void ASGAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	const auto AimActor = GetFocusOnActor();
	SetFocus(AimActor);
}

AActor* ASGAIController::GetFocusOnActor() const
{
	if(!GetBlackboardComponent()) return nullptr;

	return Cast<AActor>(GetBlackboardComponent()->GetValueAsObject(FocusOnKeyName));
}
