// My Shoot Game C++


#include "AI/SGAmmoDecoratorDecorator.h"
#include "Util.h"
#include "AI/SGAIController.h"
#include "Weapon/SGWeaponComponent.h"

USGAmmoDecoratorDecorator::USGAmmoDecoratorDecorator()
{
	NodeName = "NeedAmmo";
}

bool USGAmmoDecoratorDecorator::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	const auto Controller = OwnerComp.GetAIOwner();
	if (!Controller) return false;

	const auto WeaponComponent = Utils::GetComponent<USGWeaponComponent>(Controller->GetPawn());
	if (!WeaponComponent) return false;

	return WeaponComponent->NeedAmmo(WeaponType);
}
