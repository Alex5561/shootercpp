// My Shoot Game C++


#include "AI/SGFindEnemyServiceService.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"
#include "Util.h"
#include "AI/SGAIPerceptionComponent.h"

USGFindEnemyServiceService::USGFindEnemyServiceService()
{
	NodeName = "FindEnemy";
}

void USGFindEnemyServiceService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{


	const auto BlackBoard = OwnerComp.GetBlackboardComponent();
	if (BlackBoard)
	{
		const auto Controller = OwnerComp.GetAIOwner();
		const auto PerceptionComponent = Utils::GetComponent<USGAIPerceptionComponent>(Controller);
		if (PerceptionComponent)
		{
			BlackBoard->SetValueAsObject(EnemyActorKey.SelectedKeyName, PerceptionComponent->GetClossesEnemy());
		}
	}
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}