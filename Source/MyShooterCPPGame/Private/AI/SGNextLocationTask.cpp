// My Shoot Game C++


#include "AI/SGNextLocationTask.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"
#include "NavigationSystem.h"

USGNextLocationTask::USGNextLocationTask()
{
	NodeName = "Next Location";
}

EBTNodeResult::Type USGNextLocationTask::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	const auto Controller = OwnerComp.GetAIOwner();
	const auto BlackBoard = OwnerComp.GetBlackboardComponent();
	if (!Controller || !BlackBoard) return EBTNodeResult::Failed;

	const auto Pawn = Controller->GetPawn();
	if (!Controller) return EBTNodeResult::Failed;

	const auto NavSystem = UNavigationSystemV1::GetCurrent(Pawn);
	if (!NavSystem) return EBTNodeResult::Failed;
	
	FNavLocation NavLocation;
	auto Location = Pawn->GetActorLocation();
	if (!SelfCenter)
	{
		auto CenterActor = Cast<AActor>(BlackBoard->GetValueAsObject(CenterKey.SelectedKeyName));
		if(!CenterActor) EBTNodeResult::Failed;
		Location = CenterActor->GetActorLocation();
	}

	const auto Fand = NavSystem->GetRandomReachablePointInRadius(Location, Radius, NavLocation);
	if (!Fand) return EBTNodeResult::Failed;

	BlackBoard->SetValueAsVector(AimLocationKey.SelectedKeyName, NavLocation.Location);
	return EBTNodeResult::Succeeded;
}
