// My Shoot Game C++


#include "AI/SGHealthPersentDecorator.h"
#include "Util.h"
#include "AI/SGAIController.h"
#include "Components/HealthComponent.h"


USGHealthPersentDecorator::USGHealthPersentDecorator()
{
	NodeName = "HealthPersent";
}

bool USGHealthPersentDecorator::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	const auto Controller = OwnerComp.GetAIOwner();
	if (!Controller) return false;

	const auto AIHealthComponent = Utils::GetComponent<UHealthComponent>(Controller->GetPawn());
	if (!AIHealthComponent || AIHealthComponent->IsDead()) return false;

	return AIHealthComponent->GetHealthPercent() <= HealthPersent;
}
