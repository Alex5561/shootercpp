// My Shoot Game C++


#include "AI/SGAICharacter.h"
#include "AI/SGAIController.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Weapon/SGAIWeaponComponent.h"
#include "BrainComponent.h"
#include "Components/WidgetComponent.h"
#include "UI/SGHealthBarWidget.h"
#include "Components/HealthComponent.h"

ASGAICharacter::ASGAICharacter(const FObjectInitializer& ObjectInit)
:  Super(ObjectInit.SetDefaultSubobjectClass<USGAIWeaponComponent>("WeaponComponentAI"))
{
	
	AutoPossessAI = EAutoPossessAI::Disabled;
	AIControllerClass = ASGAIController::StaticClass();

	

	bUseControllerRotationYaw = false;
	if (GetCharacterMovement())
	{
		GetCharacterMovement()->bUseControllerDesiredRotation = true;
		GetCharacterMovement()->RotationRate = FRotator(0.f, 200.f, 0.f);
	}

	HealthBarWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("HealthBarWidget"));
	HealthBarWidget->SetupAttachment(GetRootComponent());
	HealthBarWidget->SetWidgetSpace(EWidgetSpace::Screen);
	HealthBarWidget->SetDrawAtDesiredSize(true);
}

void ASGAICharacter::BeginPlay()
{
	Super::BeginPlay();

	check(HealthBarWidget);
}

void ASGAICharacter::OnDeath()
{
	Super::OnDeath();
	
	const auto SGController = Cast<AAIController>(Controller);
	if (SGController && SGController->BrainComponent)
	{
		SGController->BrainComponent->Cleanup();
	}
	const auto HealthBarWid = Cast<USGHealthBarWidget>(HealthBarWidget->GetUserWidgetObject());
	if (!HealthBarWid) return;

	HealthBarWid->SetVisibility(ESlateVisibility::Hidden);
}

void ASGAICharacter::OnhealthChange(float HealthValue, float HealthDelta)
{
	Super::OnhealthChange(HealthValue, HealthDelta);

	const auto HealthBarWid = Cast<USGHealthBarWidget>(HealthBarWidget->GetUserWidgetObject());
	if (!HealthBarWid) return;

	HealthBarWid->SetHealthPercentBar(HealthComponent->GetHealthPercent());
}