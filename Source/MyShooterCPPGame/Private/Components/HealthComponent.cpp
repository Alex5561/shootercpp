// My Shoot Game C++


#include "Components/HealthComponent.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/Controller.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "Camera/CameraShake.h"
#include "SGGameModeBase.h"
#include "Perception/AISense_Damage.h"


// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	
	PrimaryComponentTick.bCanEverTick = false;
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	SetHealth(MaxHealth);
	

	AActor* Component = GetOwner();
	if (Component)
	{
		Component->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::OnTakeAnyDamageHandle);
	}
	
}

void UHealthComponent::ChangeHealth(float Value)
{
	SetHealth(Health + Value);
}

void UHealthComponent::OnTakeAnyDamageHandle(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (Damage <= 0.0f || IsDead() || !GetWorld()) return;

	SetHealth(Health - Damage);

	if (IsDead())
	{
		Killed(InstigatedBy);
		OnDead.Broadcast();
	}
	else if (AutoHeal)
	{
		GetWorld()->GetTimerManager().SetTimer(HealTimerHAndle, this, &UHealthComponent::HealUpdate, HealUpdateTime, true, HealDelay);
	}
	PlayCameraShake();
	ReportDamage(Damage, InstigatedBy);
}

void UHealthComponent::HealUpdate()
{
	SetHealth(Health+HealModifaier);
	
	if (FMath::IsNearlyEqual(Health,MaxHealth) && GetWorld())
	{
		GetWorld()->GetTimerManager().ClearTimer(HealTimerHAndle);
	}

}

void UHealthComponent::SetHealth(float HealthVal)
{
	const auto NextHealth = FMath::Clamp(HealthVal, 0.0f, MaxHealth);
	const auto HealthDelta = NextHealth - Health;
	Health = NextHealth;
	OnHealthChange.Broadcast(Health, HealthDelta);
}

void UHealthComponent::PlayCameraShake()
{
	if (IsDead()) return;

	const auto Player = Cast<APawn>(GetOwner());
	if (!Player) return;

	const auto Contoroller = Player->GetController<APlayerController>();
	if (!Contoroller || !Contoroller->PlayerCameraManager) return;

	Contoroller->PlayerCameraManager->StartCameraShake(ShakeClass);


}

void UHealthComponent::Killed(AController* KillerController)
{
	const auto GameMode = Cast<ASGGameModeBase>(GetWorld()->GetAuthGameMode());
	if (!GameMode) return;

	const auto Player = Cast<APawn>(GetOwner());
	const auto VicmitController = Player ? Player->Controller : nullptr;
	GameMode->Killed(KillerController, VicmitController);

}

void UHealthComponent::ReportDamage(float Damage, AController* InstigatorBy)
{
	if (!InstigatorBy || !InstigatorBy->GetPawn() || !GetOwner()) return;
	UAISense_Damage::ReportDamageEvent(GetWorld(), GetOwner(), InstigatorBy->GetPawn(), Damage, InstigatorBy->GetPawn()->GetActorLocation(), GetOwner()->GetActorLocation());
}




