// My Shoot Game C++


#include "Player/SGBaseCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/HealthComponent.h"
#include "Components/TextRenderComponent.h"
#include "GameFramework/Controller.h"
#include "Weapon/SGWeaponComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SphereComponent.h"


ASGBaseCharacter::ASGBaseCharacter(const FObjectInitializer& ObjectInit) : Super(ObjectInit)
{
 	
	PrimaryActorTick.bCanEverTick = true;


	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComponent"));
	SpringArmComponent->SetupAttachment(GetRootComponent());
	SpringArmComponent->bUsePawnControlRotation = true;
	SpringArmComponent->TargetArmLength = 300.f;
	SpringArmComponent->SocketOffset = FVector(0, 100.f, 80.f);

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetupAttachment(SpringArmComponent);

	CameraCollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("CameraCollion"));
	CameraCollisionComponent->SetupAttachment(CameraComponent);
	CameraCollisionComponent->SetSphereRadius(10.f);
	CameraCollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Health"));

	TextHealth = CreateDefaultSubobject<UTextRenderComponent>(TEXT("ReenderHealth"));
	TextHealth->SetupAttachment(GetRootComponent());
	TextHealth->SetRelativeLocation(FVector(0.f, 0.f, 90.f));
	TextHealth->SetHorizontalAlignment(EHorizTextAligment::EHTA_Center);
	TextHealth->bOwnerNoSee = true;

	WeaponComponent = CreateDefaultSubobject<USGWeaponComponent>(TEXT("WeaponComp"));

}


void ASGBaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	check(HealthComponent);
	check(TextHealth);

	OnhealthChange(HealthComponent->GetHealth(),0.f);
	HealthComponent->OnDead.AddUObject(this, &ASGBaseCharacter::OnDeath);
	HealthComponent->OnHealthChange.AddUObject(this, &ASGBaseCharacter::OnhealthChange);
	
	LandedDelegate.AddDynamic(this, &ASGBaseCharacter::OnGroundLanded);

	CameraCollisionComponent->OnComponentBeginOverlap.AddDynamic(this, &ASGBaseCharacter::OnCameraBeginOverlap);
	CameraCollisionComponent->OnComponentEndOverlap.AddDynamic(this, &ASGBaseCharacter::OnCameraEndOverrlap);
}


void ASGBaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

void ASGBaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Forward", this, &ASGBaseCharacter::MoveForward);
	PlayerInputComponent->BindAxis("Right", this, &ASGBaseCharacter::MoveRight);

	PlayerInputComponent->BindAxis("LookUP", this, &ASGBaseCharacter::LookUP);
	PlayerInputComponent->BindAxis("Turn", this, &ASGBaseCharacter::Turn);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ASGBaseCharacter::Jump);
	PlayerInputComponent->BindAction("Run", IE_Pressed, this, &ASGBaseCharacter::OnSprint);
	PlayerInputComponent->BindAction("Run", IE_Released, this, &ASGBaseCharacter::UnSprint);
	PlayerInputComponent->BindAction("Fire", IE_Pressed, WeaponComponent, &USGWeaponComponent::StartFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, WeaponComponent, &USGWeaponComponent::StopFire);
	PlayerInputComponent->BindAction("SwitchWeapon", IE_Pressed, WeaponComponent, &USGWeaponComponent::NextWeapon);
	PlayerInputComponent->BindAction("Reload", IE_Pressed, WeaponComponent, &USGWeaponComponent::Reload);
}


float ASGBaseCharacter::CalculateDirectionABP() const
{
	if (GetVelocity().IsZero()) return 0.0f;
	FVector VelocityNorm = GetVelocity().GetSafeNormal();
	float AngelBeetwen = FMath::Acos(FVector::DotProduct(GetActorForwardVector(), VelocityNorm));
	FVector Cross = FVector::CrossProduct(GetActorForwardVector(), VelocityNorm);
	float Degrees = FMath::RadiansToDegrees(AngelBeetwen);

	return  Cross.IsZero() ? Degrees : Degrees * FMath::Sign(Cross.Z);
}

void ASGBaseCharacter::SetPlayerColor(const FLinearColor& Color)
{
	const auto MaterialInstance = GetMesh()->CreateAndSetMaterialInstanceDynamic(0);
	if (!MaterialInstance) return;

	MaterialInstance->SetVectorParameterValue(MaterialColorName, Color);
}



void ASGBaseCharacter::MoveForward(float Amount)
{
	isForward = Amount > 0.f;
	AddMovementInput(this->GetActorForwardVector(), Amount);
}

void ASGBaseCharacter::MoveRight(float Amount)
{
	AddMovementInput(this->GetActorRightVector(), Amount);
}

void ASGBaseCharacter::LookUP(float Amount)
{
	AddControllerPitchInput(Amount * -0.5f);
}

void ASGBaseCharacter::Turn(float Amount)
{
	AddControllerYawInput(Amount);
}

void ASGBaseCharacter::OnSprint()
{
	if (!GetCharacterMovement()->IsFalling() && isForward && !GetVelocity().IsZero())
	{
		isSprint = true;
		GetCharacterMovement()->MaxWalkSpeed = 800.f;
	}
}

void ASGBaseCharacter::UnSprint()
{
	isSprint = false;
	GetCharacterMovement()->MaxWalkSpeed = 600.f;
}

void ASGBaseCharacter::OnDeath()
{
	PlayAnimMontage(DeathMontage);

	GetCharacterMovement()->DisableMovement();
	GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	SetLifeSpan(5.0f);

	if (Controller)
	{
		Controller->ChangeState(NAME_Spectating);
	}
	WeaponComponent->StopFire();

	//GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	//GetMesh()->SetSimulatePhysics(true);
}

void ASGBaseCharacter::OnhealthChange(float HealthValue, float HealthDelta)
{
	TextHealth->SetText(FText::FromString(FString::Printf(TEXT("%.0f"), HealthValue)));
}

void ASGBaseCharacter::OnGroundLanded(const FHitResult& HitResult)
{
	const float FallVelocityZ = -GetCharacterMovement()->Velocity.Z;
	if (FallVelocityZ < LandedDamageVelocity.X) return;

	const float FinalDamage = FMath::GetMappedRangeValueClamped(LandedDamageVelocity, LandedDamage, FallVelocityZ);
	TakeDamage(FinalDamage, FDamageEvent{}, nullptr, nullptr);
}

void ASGBaseCharacter::OnCameraBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	CheckCameraOverlap();
}

void ASGBaseCharacter::OnCameraEndOverrlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	CheckCameraOverlap();
}

void ASGBaseCharacter::CheckCameraOverlap()
{
	const auto HideMesh = CameraCollisionComponent->IsOverlappingComponent(GetCapsuleComponent());
	GetMesh()->SetOwnerNoSee(HideMesh);
}


void ASGBaseCharacter::TurnOff()
{
	WeaponComponent->StopFire();
	Super::TurnOff();
}

void ASGBaseCharacter::Reset()
{
	WeaponComponent->StopFire();
	Super::Reset();
}


