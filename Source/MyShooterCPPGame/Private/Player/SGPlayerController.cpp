// My Shoot Game C++


#include "Player/SGPlayerController.h"
#include "Player/SGBaseCharacter.h"
#include "AI/SGRespawnComponent.h"
#include "SGGameModeBase.h"

ASGPlayerController::ASGPlayerController()
{
	SGRespawnComponent = CreateDefaultSubobject<USGRespawnComponent>(TEXT("RespawnComponent"));
}

void ASGPlayerController::BeginPlay()
{
	Super::BeginPlay();
	if (GetWorld() && GetWorld()->GetAuthGameMode())
	{
		const auto GameMode = Cast<ASGGameModeBase>(GetWorld()->GetAuthGameMode());
		GameMode->OnMatchStateChange.AddUObject(this, &ASGPlayerController::OnMatchStateChange);
	}
	
}

void ASGPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	OnNewPawn.Broadcast(InPawn);
}

void ASGPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	if (!InputComponent) return;

	InputComponent->BindAction("PauseGame", IE_Pressed, this, &ASGPlayerController::OnPauseGame);
}

void ASGPlayerController::OnPauseGame()
{
	if (!GetWorld() || !GetWorld()->GetAuthGameMode()) return;

	GetWorld()->GetAuthGameMode()->SetPause(this);
}

void ASGPlayerController::OnMatchStateChange(ESGMathState State)
{
	if (State == ESGMathState::InProgress)
	{
		SetInputMode(FInputModeGameOnly());
		bShowMouseCursor = false;
	}
	else
	{
		SetInputMode(FInputModeUIOnly());
		bShowMouseCursor = true;
	}
}
