// My Shoot Game C++


#include "SGGameModeBase.h"
#include "Player/SGBaseCharacter.h"
#include "Player/SGPlayerController.h"
#include "UI/SGHUD.h"
#include "AI/SGAIController.h"
#include "AI/SGPlayerState.h"
#include "AI/SGRespawnComponent.h"
#include "Util.h"
#include "EngineUtils.h"
#include "Weapon/SGWeaponComponent.h"

constexpr static int32 MinRoundTimeForRespawn = 5;

ASGGameModeBase::ASGGameModeBase()
{
	DefaultPawnClass = ASGBaseCharacter::StaticClass();
	PlayerControllerClass = ASGPlayerController::StaticClass();
	HUDClass = ASGHUD::StaticClass();
	PlayerStateClass = ASGPlayerState::StaticClass();
}

void ASGGameModeBase::StartPlay()
{
	Super::StartPlay();

	SpawnBots();
	CreateTeamsInfo();
	CurrentRound = 1;
	StartRound();
	SetMatchState(ESGMathState::InProgress);
}

UClass* ASGGameModeBase::GetDefaultPawnClassForController_Implementation(AController* InController)
{
	if (InController && InController->IsA<ASGAIController>())
	{
		return AIPawnClass;
	}

	return Super::GetDefaultPawnClassForController_Implementation(InController);
}

void ASGGameModeBase::Killed(AController* KillerController, AController* VikcingController)
{
	const auto KillerPlayerState = KillerController ? Cast<ASGPlayerState>(KillerController->PlayerState) : nullptr;
	const auto VikcingPlayerState = VikcingController ? Cast<ASGPlayerState>(VikcingController->PlayerState) : nullptr;

	if (KillerPlayerState)
	{
		KillerPlayerState->AddKill();
	}
	if (VikcingPlayerState)
	{
		VikcingPlayerState->AddDeath();
	}
	StartRespawn(VikcingController);
}




void ASGGameModeBase::SpawnBots()
{
	if (!GetWorld()) return;

	for (int32 i = 0; i < GameData.PlayersNum - 1; i++)
	{
		FActorSpawnParameters SpawnInfo;
		SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		const auto SGAIController = GetWorld()->SpawnActor<ASGAIController>(AIControllerClass, SpawnInfo);
		RestartPlayer(SGAIController);
	}
}

void ASGGameModeBase::StartRound()
{
	RoundCountDownn = GameData.RoundTime;
	GetWorldTimerManager().SetTimer(GameRoundTimerHandle, this, &ASGGameModeBase::GameTimerUpdate, 1.f, true);
}

void ASGGameModeBase::GameTimerUpdate()
{
	if (--RoundCountDownn == 0)
	{
		GetWorldTimerManager().ClearTimer(GameRoundTimerHandle);
		if (CurrentRound + 1 <= GameData.RoundNum)
		{
			++CurrentRound;
			ResetPlayers();
			StartRound();
		}
		else
		{
			GameOver();
		}
	}
}

void ASGGameModeBase::ResetPlayers()
{
	if (!GetWorld()) return;

	for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		ResetOnePlayer(It->Get());
	}
}

void ASGGameModeBase::ResetOnePlayer(AController* Controller)
{
	if (Controller && Controller->GetPawn())
	{
		Controller->GetPawn()->Reset();
	}
	RestartPlayer(Controller);
	SetPlayerColor(Controller);
}

void ASGGameModeBase::CreateTeamsInfo()
{
	if (!GetWorld()) return;

	int32 TeamID = 1;
	for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		const auto Controller = It->Get();
		if (!Controller) continue;

		const auto  PlayerState = Cast<ASGPlayerState>(Controller->PlayerState);
		if (!PlayerState) continue;

		PlayerState->SetTeamID(TeamID);
		PlayerState->SetTeamColor(DetermineColorByTeamID(TeamID));
		PlayerState->SetPlayerName(Controller->IsPlayerController() ? "Player" : "Bot");
		SetPlayerColor(Controller);
		TeamID = TeamID == 1 ? 2 : 1;
	}
}

FLinearColor ASGGameModeBase::DetermineColorByTeamID(int32 TeamID) const
{
	if (TeamID - 1 < GameData.TeamColors.Num())
	{
		return GameData.TeamColors[TeamID - 1];
	}

	return GameData.DefaultTeamColor;
}

void ASGGameModeBase::SetPlayerColor(AController* Controller)
{
	if (!Controller) return;

	const auto Character = Cast<ASGBaseCharacter>(Controller->GetPawn());
	if (!Character) return;

	const auto PlayerState = Cast<ASGPlayerState>(Controller->PlayerState);
	if (!PlayerState) return;

	Character->SetPlayerColor(PlayerState->GetTeamColor());

}

void ASGGameModeBase::StartRespawn(AController* Controller)
{
	const auto RespawnAviable = RoundCountDownn > MinRoundTimeForRespawn + GameData.RespawnTime;
	if (!RespawnAviable) return;
	const auto RespawnComponent = Utils::GetComponent<USGRespawnComponent>(Controller);
	if (!RespawnComponent) return;

	RespawnComponent->Respawn(GameData.RespawnTime);
}

void ASGGameModeBase::GameOver()
{
	for (auto Pawn : TActorRange<APawn>(GetWorld()))
	{
		if (Pawn)
		{
			Pawn->TurnOff();
			Pawn->DisableInput(nullptr);

		}
	}
	SetMatchState(ESGMathState::GameOver);
}

void ASGGameModeBase::SetMatchState(ESGMathState State)
{
	if (MatchState == State) return;
	MatchState = State;
	OnMatchStateChange.Broadcast(MatchState);
}

void ASGGameModeBase::StopAllFire()
{
	for (auto Pawn : TActorRange<APawn>(GetWorld()))
	{
		const auto WeaponComponent = Utils::GetComponent<USGWeaponComponent>(Pawn);
		if (!WeaponComponent) continue;
		WeaponComponent->StopFire();
	}
}

void ASGGameModeBase::RespawnRequest(AController* Controller)
{
	ResetOnePlayer(Controller);
}

bool ASGGameModeBase::SetPause(APlayerController* PC, FCanUnpause CanUnpauseDelegate)
{
	const auto PauseState = Super::SetPause(PC, CanUnpauseDelegate);
	if (PauseState)
	{
		SetMatchState(ESGMathState::Pause);
		StopAllFire();
	}
	return PauseState;
}

bool ASGGameModeBase::ClearPause()
{
	const auto ClearPaused = Super::ClearPause();
	if (ClearPaused)
	{
		SetMatchState(ESGMathState::InProgress);
	}
	return ClearPaused;
}

