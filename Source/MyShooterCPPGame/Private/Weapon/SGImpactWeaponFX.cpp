// My Shoot Game C++


#include "Weapon/SGImpactWeaponFX.h"
#include "NiagaraFunctionLibrary.h"
#include "Kismet/GamePlayStatics.h"
#include "Components/DecalComponent.h"
#include "Sound/SoundCue.h"

USGImpactWeaponFX::USGImpactWeaponFX()
{
	
	PrimaryComponentTick.bCanEverTick = false;
}



void USGImpactWeaponFX::PlayImpactFX(const FHitResult Hit)
{
	auto ImpactData = DefaultImpactData;

	if (Hit.PhysMaterial.IsValid())
	{
		const auto Physic = Hit.PhysMaterial.Get();
		if (ImpactDataMap.Contains(Physic))
		{
			ImpactData = ImpactDataMap[Physic];
		}
	}

	UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(),//
		ImpactData.NiagaraEffect,//
		Hit.ImpactPoint,//
		Hit.ImpactNormal.Rotation());
	auto DecalComponent = UGameplayStatics::SpawnDecalAtLocation(GetWorld(),//
		ImpactData.DecalData.Material,//
		ImpactData.DecalData.SIze,//
		Hit.ImpactPoint, Hit.Normal.Rotation());
	if (DecalComponent)
	{
		DecalComponent->SetFadeOut(ImpactData.DecalData.LifeTime, ImpactData.DecalData.FadeOutTime);
	}
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), ImpactData.ImpactSound, Hit.ImpactPoint);


}
