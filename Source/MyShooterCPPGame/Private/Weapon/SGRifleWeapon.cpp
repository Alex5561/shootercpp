// My Shoot Game C++


#include "Weapon/SGRifleWeapon.h"
#include "DrawDebugHelpers.h"
#include "Weapon/SGImpactWeaponFX.h"
#include "Engine/World.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"
#include "Components/AudioComponent.h"

ASGRifleWeapon::ASGRifleWeapon()
{
	FXComponent = CreateDefaultSubobject<USGImpactWeaponFX>(TEXT("FXComponent"));
}

void ASGRifleWeapon::BeginPlay()
{
	Super::BeginPlay();
	check(FXComponent);
}

void ASGRifleWeapon::StopFire()
{
	GetWorldTimerManager().ClearTimer(ShotTimerHAndle);
	SetMuzzleFXVisibility(false);
}

void ASGRifleWeapon::MakeShot()
{
	if (!GetWorld() || IsAmmoEmpty())
	{
		StopFire();
		return;
	}

	FVector TraceStart, TraceEnd;
	if (!GetTraceData(TraceStart, TraceEnd))
	{
		StopFire();
		return;
	}

	FVector TraceEndFX = TraceEnd;

	FHitResult HitResult;
	MakeHit(HitResult, TraceStart, TraceEnd);
	MakeDamage(HitResult);
	if (HitResult.bBlockingHit)
	{
		TraceEndFX = HitResult.ImpactPoint;
		//DrawDebugSphere(GetWorld(), HitResult.ImpactPoint, 5.0f, 24, FColor::Red, false, 5.0f);
		//DrawDebugLine(GetWorld(), GetMuzzleWorldLocation(), HitResult.ImpactPoint, FColor::Red, false, 3.0f, 0, 3.0f);
		FXComponent->PlayImpactFX(HitResult);
	}
	SpawnTraceFX(GetMuzzleWorldLocation(), TraceEndFX);
	DecreaseAmmo();
}

void ASGRifleWeapon::StartFire()
{
	InitMuzzleFX();
	GetWorldTimerManager().SetTimer(ShotTimerHAndle, this, &ASGBaseWeapon::MakeShot, RateShot, true);
	MakeShot();
}

bool ASGRifleWeapon::GetTraceData(FVector& StartTrace, FVector& EndTrace)
{
	FVector ViemLocation;
	FRotator ViemRotation;
	if (!GetPlayerViemPoint(ViemLocation, ViemRotation)) return false;

	StartTrace = ViemLocation;
	const auto HalfCone = FMath::DegreesToRadians(RandomShot);
	const FVector ShootDirection = FMath::VRandCone(ViemRotation.Vector(), HalfCone);
	EndTrace = StartTrace + ShootDirection * 10050.0f;

	return true;
}

void ASGRifleWeapon::InitMuzzleFX()
{
	if (!MuzzleFXComponent)
	{
		MuzzleFXComponent = SpawnMuzzleFX();
	}
	if (!FireAudioComponent)
	{
		FireAudioComponent = UGameplayStatics::SpawnSoundAttached(FireSound,WeaponMesh,MuzzleSocket);
	}
	SetMuzzleFXVisibility(true);
}

void ASGRifleWeapon::SetMuzzleFXVisibility(bool Visibility)
{
	if (MuzzleFXComponent)
	{
		MuzzleFXComponent->SetPaused(!Visibility);
		MuzzleFXComponent->SetVisibility(Visibility, true);
	}
	if (FireAudioComponent)
	{
		Visibility ? FireAudioComponent->Play() : FireAudioComponent->Stop();
	}
}

void ASGRifleWeapon::SpawnTraceFX(const FVector& StartTrace, const FVector& EndTrace)
{
	const auto TraceFXComponent = UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), TraceFX, StartTrace);
	if (TraceFXComponent)
	{
		TraceFXComponent->SetNiagaraVariableVec3(TraceTargetName, EndTrace);
	}
}





