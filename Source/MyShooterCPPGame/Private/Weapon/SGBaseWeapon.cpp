// My Shoot Game C++


#include "Weapon/SGBaseWeapon.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
#include "GameFrameWork/Character.h"
#include "GameFrameWork/Controller.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"



ASGBaseWeapon::ASGBaseWeapon()
{
 	
	PrimaryActorTick.bCanEverTick = false;

	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("MeshWeapon"));
	SetRootComponent(WeaponMesh);


}

void ASGBaseWeapon::DecreaseAmmo()
{
	if (CurrentAmmo.Bullets == 0)
	{
		return;
	}
	CurrentAmmo.Bullets--;
	if (IsClipEmpty() && !IsAmmoEmpty())
	{
		StopFire();
		OnClipEmpty.Broadcast(this);
	}
	
}

bool ASGBaseWeapon::IsAmmoEmpty() const
{
	return !CurrentAmmo.Infinite && CurrentAmmo.Clips == 0 && IsClipEmpty() ;
}

bool ASGBaseWeapon::IsClipEmpty() const
{
	return CurrentAmmo.Bullets==0;
}

void ASGBaseWeapon::ChangeClip()
{
	if (!CurrentAmmo.Infinite)
	{
		if (CurrentAmmo.Clips == 0)
		{
			return;
		}
		CurrentAmmo.Clips--;
	}
	CurrentAmmo.Bullets = DefaultAmmo.Bullets;
}

bool ASGBaseWeapon::CanReload() const
{
	return CurrentAmmo.Bullets < DefaultAmmo.Bullets && CurrentAmmo.Clips > 0;
}

bool ASGBaseWeapon::TrytoAddAmmo(int32 ClipsAmount)
{
	if (CurrentAmmo.Infinite || IsAmmoFool() || ClipsAmount <= 0) return false;

	if (IsAmmoEmpty())
	{
		CurrentAmmo.Clips = FMath::Clamp(CurrentAmmo.Clips + ClipsAmount, 0, DefaultAmmo.Clips + 1);
		OnClipEmpty.Broadcast(this);
	}
	else if (CurrentAmmo.Clips < DefaultAmmo.Clips)
	{
		const auto NextClipAmount = CurrentAmmo.Clips + ClipsAmount;
		if (DefaultAmmo.Clips - NextClipAmount >= 0)
		{
			CurrentAmmo.Clips = NextClipAmount;
		}
		else
		{
			CurrentAmmo.Clips = DefaultAmmo.Clips;
			CurrentAmmo.Bullets = DefaultAmmo.Bullets;
		}
	}
	else
	{
		CurrentAmmo.Bullets = DefaultAmmo.Bullets;
	}
	return true;
}

UNiagaraComponent* ASGBaseWeapon::SpawnMuzzleFX()
{
	return UNiagaraFunctionLibrary::SpawnSystemAttached(MuzzleFX,//
		WeaponMesh,//
		MuzzleSocket,//
		FVector::ZeroVector,//
		FRotator::ZeroRotator,//
		EAttachLocation::SnapToTarget,//
		true);
}

AController* ASGBaseWeapon::GetController()
{

	const auto Pawn = Cast<APawn>(GetOwner());

	return Pawn ? Pawn->GetController() : nullptr;
}

bool ASGBaseWeapon::IsAmmoFool() const
{
	return CurrentAmmo.Clips == DefaultAmmo.Clips && CurrentAmmo.Bullets == DefaultAmmo.Bullets;
}

void ASGBaseWeapon::BeginPlay()
{
	Super::BeginPlay();
	check(WeaponMesh);
	CurrentAmmo = DefaultAmmo;
}

void ASGBaseWeapon::StartFire()
{
	//
}

void ASGBaseWeapon::StopFire()
{
	//
}

void ASGBaseWeapon::MakeShot()
{
	//
}



bool ASGBaseWeapon::GetPlayerViemPoint(FVector& Location, FRotator& Rotation) 
{
	const auto SGCharacter = Cast<ACharacter>(GetOwner());
	if (!SGCharacter) return false;
	
	if (SGCharacter->IsPlayerControlled())
	{
		const auto Controller = SGCharacter->GetController<APlayerController>();
		if (!Controller) return false;
		Controller->GetPlayerViewPoint(Location, Rotation);
	}
	else
	{
		Location = GetMuzzleWorldLocation();
		Rotation = WeaponMesh->GetSocketRotation(MuzzleSocket);
	}
	return true;
}

FVector ASGBaseWeapon::GetMuzzleWorldLocation() const
{
	return WeaponMesh->GetSocketLocation(MuzzleSocket);
}

bool ASGBaseWeapon::GetTraceData(FVector& StartTrace, FVector& EndTrace)
{
	FVector ViemLocation;
	FRotator ViemRotation;
	if(!GetPlayerViemPoint(ViemLocation, ViemRotation)) return false;

	StartTrace = ViemLocation; 
	const FVector ShootDirection = ViemRotation.Vector();
	EndTrace = StartTrace + ShootDirection * 10050.0f;
	
	return true;
}

void ASGBaseWeapon::MakeHit(FHitResult& HitResult, const FVector& TraceStart, const FVector& TraceEnd)
{
	if (!GetWorld()) return;

	FCollisionQueryParams CollisionParams;
	CollisionParams.AddIgnoredActor(GetOwner());
	CollisionParams.bReturnPhysicalMaterial = true;
	GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, ECollisionChannel::ECC_Visibility, CollisionParams);

}

void ASGBaseWeapon::MakeDamage(FHitResult& ResultHit)
{
	const auto ActorHit = ResultHit.GetActor();
	if (!ActorHit) return;

	ActorHit->TakeDamage(DamageValue, FDamageEvent(), GetController(), this);

}


