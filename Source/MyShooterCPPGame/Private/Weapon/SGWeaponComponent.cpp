// My Shoot Game C++


#include "Weapon/SGWeaponComponent.h"
#include "Weapon/SGBaseWeapon.h"
#include "GameFrameWork/Character.h"
#include "SGEquipFinishedAnimNotify.h"
#include "SGReloadFinishAnimNotify.h"


USGWeaponComponent::USGWeaponComponent()
{

	PrimaryComponentTick.bCanEverTick = false;
}



void USGWeaponComponent::BeginPlay()
{
	Super::BeginPlay();

	InitAnimations();
	SpawnWeapons();
	EquipWeapon(CurrentWeaponIndex);
	
}

void USGWeaponComponent::SpawnWeapons()
{
	
	ACharacter* Char = Cast<ACharacter>(GetOwner());
	if (!GetWorld() || !Char) return;
	
	
	for (auto OnWeaponData : WeaponData)
	{
		auto Weapon = GetWorld()->SpawnActor<ASGBaseWeapon>(OnWeaponData.WeaponClass);
		if (!Weapon) continue;

		Weapon->OnClipEmpty.AddUObject(this, &USGWeaponComponent::OnEmptyClip);
		Weapon->SetOwner(Char);
		Weapons.Add(Weapon);

		AttachWeaponToSocket(Weapon, Char->GetMesh(), WeaponArmorySocketName);
	}
}

void USGWeaponComponent::AttachWeaponToSocket(ASGBaseWeapon* Weapon, USceneComponent* Component, const FName& Socket)
{
	if (!Weapon || !Component) return;
	FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, false);
	Weapon->AttachToComponent(Component, AttachmentRules, Socket);
}

void USGWeaponComponent::EquipWeapon(int32 WeaponIndex)
{
	if (WeaponIndex < 0 || WeaponIndex >= Weapons.Num()) return;

	ACharacter* Char = Cast<ACharacter>(GetOwner());
	if (!Char) return;

	if (CurrentWeapon)
	{
		CurrentWeapon->StopFire();
		AttachWeaponToSocket(CurrentWeapon, Char->GetMesh(), WeaponArmorySocketName);
	}


	CurrentWeapon = Weapons[CurrentWeaponIndex];
	//CurrentReloadAnimMOntage = WeaponData[CurrentWeaponIndex].ReloadAnim;
	const auto CurrentWeaponData = WeaponData.FindByPredicate([&](const FWeaponData& Data) {return Data.WeaponClass == CurrentWeapon->GetClass(); });
	CurrentReloadAnimMOntage = CurrentWeaponData ? CurrentWeaponData->ReloadAnim : nullptr;
	AttachWeaponToSocket(CurrentWeapon, Char->GetMesh(), WeaponEquipSocketName);
	EquipAnimToProgress = true;
	PlayAnimMontage(EquipMontage);
}

bool USGWeaponComponent::NeedAmmo(TSubclassOf<ASGBaseWeapon> WeaponType)
{
	for (auto Weapon : Weapons)
	{
		if (Weapon && Weapon->IsA(WeaponType))
		{
			return !Weapon->IsAmmoFool();
		}
	}
	return false;
}

void USGWeaponComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	CurrentWeapon = nullptr;
	for (auto Weapon : Weapons)
	{
		Weapon->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		Weapon->Destroy();
	}
	Weapons.Empty();
	Super::EndPlay(EndPlayReason);
}

void USGWeaponComponent::PlayAnimMontage(UAnimMontage* Montage)
{
	ACharacter* Char = Cast<ACharacter>(GetOwner());
	if (!Char) return;
	Char->PlayAnimMontage(Montage);
}

void USGWeaponComponent::InitAnimations()
{
	
	auto EquipFinishNotify = FindNotifyByClass<USGEquipFinishedAnimNotify>(EquipMontage);
	if (EquipFinishNotify)
	{
		EquipFinishNotify->OnNotifySignature.AddUObject(this, &USGWeaponComponent::OnEquipFinished);
	}

	for (auto OneWeaponData : WeaponData)
	{
		auto ReloadFinishNotify = FindNotifyByClass<USGReloadFinishAnimNotify>(OneWeaponData.ReloadAnim);
		if (!ReloadFinishNotify)continue;
		
		ReloadFinishNotify->OnNotifySignature.AddUObject(this, &USGWeaponComponent::OnReloadFinished);
		
	}

}

void USGWeaponComponent::OnEquipFinished(USkeletalMeshComponent* MeshComp)
{
	ACharacter* Char = Cast<ACharacter>(GetOwner());
	if (!Char && MeshComp != Char->GetMesh()) return;
	EquipAnimToProgress = false;
}

void USGWeaponComponent::OnReloadFinished(USkeletalMeshComponent* MeshComp)
{
	ACharacter* Char = Cast<ACharacter>(GetOwner());
	if (!Char && MeshComp != Char->GetMesh()) return;
	ReloadAnimToProgress = false;
}

bool USGWeaponComponent::CanFire()
{
	return  CurrentWeapon && !EquipAnimToProgress && !ReloadAnimToProgress;
}

bool USGWeaponComponent::CanEquip()
{
	return !EquipAnimToProgress && !ReloadAnimToProgress;
}

bool USGWeaponComponent::CanReload()
{
	return  CurrentWeapon && !EquipAnimToProgress && !ReloadAnimToProgress && CurrentWeapon->CanReload();
}

void USGWeaponComponent::OnEmptyClip(ASGBaseWeapon* AmmoEmptyWeapon)
{
	if (!AmmoEmptyWeapon) return;

	if (CurrentWeapon == AmmoEmptyWeapon)
	{
		ChangeClip();
	}
	else
	{
		for (auto Weapon : Weapons)
		{
			if (Weapon == AmmoEmptyWeapon)
			{
				Weapon->ChangeClip();
			}
		}
	}
}

void USGWeaponComponent::ChangeClip()
{
	if (!CanReload()) return;

	CurrentWeapon->StopFire();
	CurrentWeapon->ChangeClip();
	ReloadAnimToProgress = true;
	PlayAnimMontage(CurrentReloadAnimMOntage);
}

void USGWeaponComponent::StartFire()
{
	if (!CanFire()) return;
	CurrentWeapon->StartFire();

}

void USGWeaponComponent::StopFire()
{
	if (!CurrentWeapon) return;
	CurrentWeapon->StopFire();
}

void USGWeaponComponent::NextWeapon()
{
	if (!CanEquip()) return;
	
	CurrentWeaponIndex = (CurrentWeaponIndex + 1) % Weapons.Num();
	EquipWeapon(CurrentWeaponIndex);
}

void USGWeaponComponent::Reload()
{
	ChangeClip();
}

bool USGWeaponComponent::GetDataUIWeapon(FWeaponUIData& UIData) const
{
	{
		if (CurrentWeapon)
		{
			UIData = CurrentWeapon->GetUIData();
			return true;
		
		}
		return false;
	}
}

bool USGWeaponComponent::GetWeaponAmmoData(FAmmoData& AmmoData) const
{
	{
		if (CurrentWeapon)
		{
			AmmoData = CurrentWeapon->GetAmmoData();
			return true;

		}
		return false;
	}
}

bool USGWeaponComponent::TryToAddAmmo(TSubclassOf<ASGBaseWeapon> WeaponType, int32 ClipsAmount)
{
	for (auto Weapon : Weapons)
	{
		if (Weapon && Weapon->IsA(WeaponType))
		{
			return Weapon->TrytoAddAmmo(ClipsAmount);
		}
	}
	return false;
}







