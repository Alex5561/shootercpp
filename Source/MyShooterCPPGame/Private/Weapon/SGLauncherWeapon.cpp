// My Shoot Game C++


#include "Weapon/SGLauncherWeapon.h"
#include "Weapon/SGProjectileLauncher.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"


void ASGLauncherWeapon::MakeShot()
{
	if (!GetWorld())
	{
		StopFire();
		return;
	}

	FVector TraceStart, TraceEnd;
	if (!GetTraceData(TraceStart, TraceEnd))
	{
		StopFire();
		return;
	}

	if (IsAmmoEmpty())
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), NoAmmoSound, GetActorLocation());
		return;
	}

	FHitResult HitResult;
	MakeHit(HitResult, TraceStart, TraceEnd);

	const FVector EndPoint = HitResult.bBlockingHit ? HitResult.ImpactPoint : TraceEnd;
	const FVector Direction = (EndPoint - GetMuzzleWorldLocation()).GetSafeNormal();



	
	const FTransform SpawnTransform(FRotator::ZeroRotator,GetMuzzleWorldLocation());
	ASGProjectileLauncher*  Projectile = GetWorld()->SpawnActorDeferred<ASGProjectileLauncher>(ProjectileClass, SpawnTransform);
	if (Projectile)
	{
		Projectile->SetShotDirection(Direction);
		Projectile->SetOwner(GetOwner());
		Projectile->FinishSpawning(SpawnTransform);
	}
	DecreaseAmmo();
	SpawnMuzzleFX();
	UGameplayStatics::SpawnSoundAttached(FireSound, WeaponMesh, MuzzleSocket);
}

void ASGLauncherWeapon::StartFire()
{
	MakeShot();
}
