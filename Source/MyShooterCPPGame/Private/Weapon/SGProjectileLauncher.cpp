// My Shoot Game C++


#include "Weapon/SGProjectileLauncher.h"
#include "Components/SphereComponent.h"
#include "GameFrameWork/ProjectileMovementComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GamePlayStatics.h"
#include "Weapon/SGImpactWeaponFX.h"


// Sets default values
ASGProjectileLauncher::ASGProjectileLauncher()
{
 	
	PrimaryActorTick.bCanEverTick = false;

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	SphereCollision->InitSphereRadius(5.f);
	SphereCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	SphereCollision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	SphereCollision->bReturnMaterialOnMove = true;
	SetRootComponent(SphereCollision);

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComponent"));
	ProjectileMovement->InitialSpeed = 3500.f;
	ProjectileMovement->ProjectileGravityScale = 0.f;

	FXComponent = CreateDefaultSubobject<USGImpactWeaponFX>(TEXT("FXComponent"));

}


void ASGProjectileLauncher::BeginPlay()
{
	Super::BeginPlay();

	check(ProjectileMovement);
	check(FXComponent);

	SphereCollision->OnComponentHit.AddDynamic(this, &ASGProjectileLauncher::OnProjectileHit);
	SphereCollision->IgnoreActorWhenMoving(GetOwner(), true);

	ProjectileMovement->Velocity = ShotDirection * ProjectileMovement->InitialSpeed;
	SetLifeSpan(5.f);
}

void ASGProjectileLauncher::OnProjectileHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!GetWorld()) return;

	ProjectileMovement->StopMovementImmediately();
	UGameplayStatics::ApplyRadialDamage(GetWorld(), AmountDamage, GetActorLocation(), RadiusDamage, UDamageType::StaticClass(), { GetOwner() }, this, GetController(), DoFullDamage);
	//DrawDebugSphere(GetWorld(), GetActorLocation(), RadiusDamage, 24, FColor::Red, false,5.f);
	FXComponent->PlayImpactFX(Hit);
	Destroy();
}

AController* ASGProjectileLauncher::GetController()
{
	const auto Pawn = Cast<APawn>(GetOwner());
	
	return Pawn ? Pawn->GetController() : nullptr;
}





