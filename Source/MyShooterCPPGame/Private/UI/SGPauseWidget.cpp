// My Shoot Game C++


#include "UI/SGPauseWidget.h"
#include "Gameframework/GameModeBase.h"
#include "Components/Button.h"


bool USGPauseWidget::Initialize()
{
	const auto InitStatus = Super::Initialize();
	if (ClearPauseButtom)
	{
		ClearPauseButtom->OnClicked.AddDynamic(this, &USGPauseWidget::OnClearPause);
	}
	return InitStatus;
}

void USGPauseWidget::OnClearPause()
{
	if (!GetWorld() || !GetWorld()->GetAuthGameMode()) return;

	GetWorld()->GetAuthGameMode()->ClearPause();
}
