// My Shoot Game C++


#include "UI/SGHUD.h"
#include "Engine/Canvas.h"
#include "Blueprint/USerWidget.h"
#include "SGGameModeBase.h"

void ASGHUD::BeginPlay()
{
	Super::BeginPlay();

	GameWidgets.Add(ESGMathState::InProgress, CreateWidget<UUserWidget>(GetWorld(), PlayerHUDWidget));
	GameWidgets.Add(ESGMathState::Pause, CreateWidget<UUserWidget>(GetWorld(), PlayerPauseWidget));
	GameWidgets.Add(ESGMathState::GameOver, CreateWidget<UUserWidget>(GetWorld(), GameOverWidget));

	for (auto GameWidgetPair : GameWidgets)
	{
		const auto GameWidget = GameWidgetPair.Value;
		if (!GameWidget) continue;

		GameWidget->AddToViewport();
		GameWidget->SetVisibility(ESlateVisibility::Hidden);
	}


	if (GetWorld())
	{
		const auto GameMode = Cast<ASGGameModeBase>(GetWorld()->GetAuthGameMode());
		if (GameMode)
		{
			GameMode->OnMatchStateChange.AddUObject(this, &ASGHUD::OnMatchStateChange);
		}
	}
}

void ASGHUD::DrawCrossHair()
{
	int32 SizeX = Canvas->SizeX;
	int32 SizeY = Canvas->SizeY;
	const TInterval<float>Center(SizeX * 0.5, SizeY * 0.5);
	const float HalfLineSize = 10.0f;
	const float LineThikness = 2.0f;
	FLinearColor LineColor = FLinearColor::Green;
	DrawLine(Center.Min - HalfLineSize, Center.Max, Center.Min + HalfLineSize,Center.Max, LineColor, LineThikness);
	DrawLine(Center.Min, Center.Max - HalfLineSize, Center.Min, Center.Max+HalfLineSize, LineColor, LineThikness);
}

void ASGHUD::OnMatchStateChange(ESGMathState State)
{
	if (CurrentWidget)
	{
		CurrentWidget->SetVisibility(ESlateVisibility::Hidden);
	}
	
	if (GameWidgets.Contains(State))
	{
		CurrentWidget = GameWidgets[State];
	}
	if (CurrentWidget)
	{
		CurrentWidget->SetVisibility(ESlateVisibility::Visible);
	}
}

void ASGHUD::DrawHUD()
{
	Super::DrawHUD();
	//DrawCrossHair();
}
