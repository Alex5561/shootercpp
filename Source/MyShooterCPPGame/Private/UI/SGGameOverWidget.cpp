// My Shoot Game C++


#include "UI/SGGameOverWidget.h"
#include "SGGameModeBase.h"
#include "AI/SGPlayerState.h"
#include "UI/SGStatWidget.h"
#include "Components/VerticalBox.h"
#include "Util.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"


void USGGameOverWidget::NativeOnInitialized()
{
	if (GetWorld())
	{
		const auto GameMode = Cast<ASGGameModeBase>(GetWorld()->GetAuthGameMode());
		if (GameMode)
		{
			GameMode->OnMatchStateChange.AddUObject(this, &USGGameOverWidget::OnMatchStateChanged);
		}
	}
	if (ResetLevelButton)
	{
		ResetLevelButton->OnClicked.AddDynamic(this, &USGGameOverWidget::OnResetLevel);
	}
}

void USGGameOverWidget::OnMatchStateChanged(ESGMathState State)
{
	if (State == ESGMathState::GameOver)
	{
		UpdatePlayerStat();
	}
}

void USGGameOverWidget::UpdatePlayerStat()
{
	if (!GetWorld() || !PlayerStatBox) return;

	PlayerStatBox->ClearChildren();

	for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		const auto Controller = It->Get();
		if (!Controller) continue;

		const auto PlayerState = Cast<ASGPlayerState>(Controller->PlayerState);
		if (!PlayerState) continue;

		const auto PlayerStatWidget = CreateWidget<USGStatWidget>(GetWorld(), StatsWidget);
		if (!PlayerStatWidget) continue;

		PlayerStatWidget->SetPlayerName(FText::FromString(PlayerState->GetPlayerName()));
		PlayerStatWidget->SetKills(Utils::TextFromInt(PlayerState->GetKillsNum()));
		PlayerStatWidget->SetDeaths(Utils::TextFromInt(PlayerState->GetDeadthNum()));
		PlayerStatWidget->SetTeam(Utils::TextFromInt(PlayerState->GetTeamID()));
		PlayerStatWidget->SetPlayerIndicatorVisibility(Controller->IsPlayerController());

		PlayerStatBox->AddChild(PlayerStatWidget);
	}
}

void USGGameOverWidget::OnResetLevel()
{
	//const FName CurrentLevel = "TestLevel";
	const FString CurrentLevel = UGameplayStatics::GetCurrentLevelName(this);
	UGameplayStatics::OpenLevel(this, FName(CurrentLevel));
}
