// My Shoot Game C++


#include "UI/SGSpectatorWidget.h"
#include "AI/SGRespawnComponent.h"
#include "Util.h"

bool USGSpectatorWidget::GetRespawnTime(int32& CountDownTime)
{
	const auto RespawnComponent = Utils::GetComponent<USGRespawnComponent>(GetOwningPlayer());
	if (!RespawnComponent || !RespawnComponent->IsRespawnInProgress()) return false;

	CountDownTime = RespawnComponent->GetRespawnTime();

	return true;
}