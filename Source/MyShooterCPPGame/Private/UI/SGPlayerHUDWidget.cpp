// My Shoot Game C++


#include "UI/SGPlayerHUDWidget.h"
#include "Weapon/SGWeaponComponent.h"
#include "Components/HealthComponent.h"
#include "Util.h"

bool USGPlayerHUDWidget::Initialize()
{
	if (GetOwningPlayer())
	{
		GetOwningPlayer()->GetOnNewPawnNotifier().AddUObject(this, &USGPlayerHUDWidget::OnNewPawn);
		OnNewPawn(GetOwningPlayerPawn());
	}
	return Super::Initialize();
}

float USGPlayerHUDWidget::GetPersentHealth() const
{
	
	const auto HealthComponent = Utils::GetComponent<UHealthComponent>(GetOwningPlayerPawn()); 
	return HealthComponent->GetHealthPercent();
}

bool USGPlayerHUDWidget::GetDataUIWeapon(FWeaponUIData& UIData) const
{
	const auto WeaponComponent = Utils::GetComponent<USGWeaponComponent>(GetOwningPlayerPawn());
	if (!WeaponComponent) return false;

	return WeaponComponent->GetDataUIWeapon(UIData);
}

bool USGPlayerHUDWidget::GetAmmoWeapon(FAmmoData& AmmoData) const
{
	auto Player = GetOwningPlayerPawn();
	if (!Player) return false;

	const auto Component = Player->GetComponentByClass(USGWeaponComponent::StaticClass());
	const auto WeaponComponent = Cast<USGWeaponComponent>(Component);
	if (!WeaponComponent) return false;

	return WeaponComponent->GetWeaponAmmoData(AmmoData);
}

bool USGPlayerHUDWidget::IsPlayerALife() const
{
	auto Player = GetOwningPlayerPawn();
	if (!Player) return false;

	const auto Component = Player->GetComponentByClass(UHealthComponent::StaticClass());
	const auto HealthComponent = Cast<UHealthComponent>(Component);
	return HealthComponent && !HealthComponent->IsDead();
}

bool USGPlayerHUDWidget::IsPlayerSpactator() const
{
	const auto Controller = GetOwningPlayer();

	return Controller && Controller->GetStateName()== NAME_Spectating;
}





void USGPlayerHUDWidget::OnHealthChange(float Health, float HealthDelta)
{
	if (HealthDelta < 0)
	{
		OnTakeDamage();
		if (!IsAnimationPlaying(DamageAnimation))
		{
			PlayAnimation(DamageAnimation);
		}
	}
}

void USGPlayerHUDWidget::OnNewPawn(APawn* NewPawn)
{
	const auto HealthComponent = Utils::GetComponent<UHealthComponent>(NewPawn);
	if (HealthComponent && !HealthComponent->OnHealthChange.IsBoundToObject(this))
	{
		HealthComponent->OnHealthChange.AddUObject(this, &USGPlayerHUDWidget::OnHealthChange);
	}
}

