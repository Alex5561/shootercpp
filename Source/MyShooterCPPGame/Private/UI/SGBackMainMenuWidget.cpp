// My Shoot Game C++


#include "UI/SGBackMainMenuWidget.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "SGGameInstance.h"

void USGBackMainMenuWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (BackMenuButton)
	{
		BackMenuButton->OnClicked.AddDynamic(this, &USGBackMainMenuWidget::OnChangeMainMenu);
	}
}

void USGBackMainMenuWidget::OnChangeMainMenu()
{
	if (!GetWorld()) return;

	const auto GameInst = GetWorld()->GetGameInstance<USGGameInstance>();
	if (!GameInst) return;

	if (GameInst->GetMenuLevel().IsNone())
	{
		return;
	}
	UGameplayStatics::OpenLevel(this, GameInst->GetMenuLevel());
}
