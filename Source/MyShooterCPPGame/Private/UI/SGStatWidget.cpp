// My Shoot Game C++


#include "UI/SGStatWidget.h"
#include "Components/TextBlock.h"
#include "Components/Image.h"

void USGStatWidget::SetPlayerName(const FText& Text)
{
	if (!PlayerNameTextBlock) return;
	PlayerNameTextBlock->SetText(Text);
}

void USGStatWidget::SetKills(const FText& Text)
{
	if (!KillsNameTextBlock) return;
	KillsNameTextBlock->SetText(Text);
}

void USGStatWidget::SetDeaths(const FText& Text)
{
	if (!DeathsTextBlock) return;
	DeathsTextBlock->SetText(Text);
}

void USGStatWidget::SetTeam(const FText& Text)
{
	if (!TeamTextBlock) return;
	TeamTextBlock->SetText(Text);
}

void USGStatWidget::SetPlayerIndicatorVisibility(bool Visible)
{
	if (!PlayerIndicatorImage) return;
	PlayerIndicatorImage->SetVisibility(Visible ? ESlateVisibility::Visible : ESlateVisibility::Hidden);
}
