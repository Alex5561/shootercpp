// My Shoot Game C++


#include "UI/SGLevelItemWidget.h"
#include "Components/Button.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"


void USGLevelItemWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (LevelSelectionButton)
	{
		LevelSelectionButton->OnClicked.AddDynamic(this, &USGLevelItemWidget::OnLevelItemClick);
	}
}

void USGLevelItemWidget::OnLevelItemClick()
{
	OnLevelSelectedSignature.Broadcast(LevelData);
}

void USGLevelItemWidget::SetLevelData(const FLevelData& Data)
{
	LevelData = Data;

	if (LevelNameTextBlock)
	{
		LevelNameTextBlock->SetText(FText::FromName(Data.LevelDisplayName));
	}
	if (LevelImage)
	{
		LevelImage->SetBrushFromTexture(Data.LevelIcon);
	}
}

void USGLevelItemWidget::SetSelected(bool isSelected)
{
	if (FrameImage)
	{
		FrameImage->SetVisibility(isSelected ? ESlateVisibility::Visible : ESlateVisibility::Hidden);
	}
}
