// My Shoot Game C++


#include "UI/SGGameDataWidget.h"
#include "SGGameModeBase.h"
#include "AI/SGPlayerState.h"

int32 USGGameDataWidget::GetKillsNum() 
{
	const auto PlayerState = GetPlayerStateOw();
	return PlayerState ? PlayerState->GetKillsNum() : 0;
}

int32 USGGameDataWidget::GetCurrentRoundNum() 
{
	const auto GameMode = GetGameModeOw();
	return GameMode ? GameMode->GetGameRound() : 0;
}

int32 USGGameDataWidget::GetTotalRoundsNum() 
{
	const auto GameMode = GetGameModeOw();
	return GameMode ? GameMode->GetData().RoundNum : 0;
}

int32 USGGameDataWidget::GetRoundSecondRemaining() 
{
	const auto GameMode = GetGameModeOw();
	return GameMode ? GameMode->GetRoundSecond() : 0;
}

ASGPlayerState* USGGameDataWidget::GetPlayerStateOw()
{
	return GetOwningPlayer() ? Cast<ASGPlayerState>(GetOwningPlayer()->PlayerState) : nullptr ;
}

ASGGameModeBase* USGGameDataWidget::GetGameModeOw()
{
	return GetWorld() ? Cast<ASGGameModeBase>(GetWorld()->GetAuthGameMode()) : nullptr;
}
