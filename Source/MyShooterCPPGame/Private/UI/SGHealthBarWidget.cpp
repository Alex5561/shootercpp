// My Shoot Game C++


#include "UI/SGHealthBarWidget.h"
#include "Components/ProgressBar.h"

void USGHealthBarWidget::SetHealthPercentBar(float Value)
{
	if (!HealthBarClass) return;
	
	const auto HealthBarVisibility = (Value > PersentVisibility || FMath::IsNearlyZero(Value)) ? ESlateVisibility::Hidden : ESlateVisibility::Visible;

	HealthBarClass->SetVisibility(HealthBarVisibility);

	const auto ColorHealth = Value > PercentColor ? GoodColor : BadColor;
	HealthBarClass->SetFillColorAndOpacity(ColorHealth);

	HealthBarClass->SetPercent(Value);
}