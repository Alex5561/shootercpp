// My Shoot Game C++


#include "PickUP/SGBasePickUP.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

// Sets default values
ASGBasePickUP::ASGBasePickUP()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollisionComponent=CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	CollisionComponent->InitSphereRadius(50.0f);
	CollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	SetRootComponent(CollisionComponent);

}

// Called when the game starts or when spawned
void ASGBasePickUP::BeginPlay()
{
	Super::BeginPlay();
	check(CollisionComponent);
}

void ASGBasePickUP::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	const auto Pawn = Cast<APawn>(OtherActor);
	if (GetPickUPTo(Pawn))
	{
		PickUPTaken();
	}
}

// Called every frame
void ASGBasePickUP::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool ASGBasePickUP::ColdBeTaken() const
{
	
	return !GetWorldTimerManager().IsTimerActive(RespawnTimerHandle);
}

bool ASGBasePickUP::GetPickUPTo(APawn* PlayerPawn)
{
	return false;
}

void ASGBasePickUP::PickUPTaken()
{
	CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	GetRootComponent()->SetVisibility(false, true);

	GetWorldTimerManager().SetTimer(RespawnTimerHandle, this, &ASGBasePickUP::Respawn, RespawnTime);
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), SoundPickUP, GetActorLocation());
}

void ASGBasePickUP::Respawn()
{
	CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	GetRootComponent()->SetVisibility(true, true);
}

