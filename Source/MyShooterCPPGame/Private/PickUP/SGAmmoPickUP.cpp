// My Shoot Game C++


#include "PickUP/SGAmmoPickUP.h"
#include "Weapon/SGWeaponComponent.h"
#include "Components/HealthComponent.h"
#include "Util.h"


bool ASGAmmoPickUP::GetPickUPTo(APawn* PlayerPawn)
{
	const auto HealthComponent = Utils::GetComponent<UHealthComponent>(PlayerPawn);
	if (!HealthComponent || HealthComponent->IsDead()) return false;

	const auto WeaponComponent = Utils::GetComponent<USGWeaponComponent>(PlayerPawn);
	if (!WeaponComponent) return false;

	return WeaponComponent->TryToAddAmmo(WeaponType,ClipsAmount);
}

