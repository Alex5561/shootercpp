// My Shoot Game C++


#include "PickUP/SGHealthPickUP.h"
#include "Components/HealthComponent.h"
#include "Util.h"



bool ASGHealthPickUP::GetPickUPTo(APawn* PlayerPawn)
{
	const auto Health = Utils::GetComponent<UHealthComponent>(PlayerPawn);
	if (!Health || FMath::IsNearlyEqual(Health->GetHealth(), Health->GetMaxHealth()) ) return false;

	Health->ChangeHealth(ValueChangeHealth);
	return true;
}






