// My Shoot Game C++

using UnrealBuildTool;

public class MyShooterCPPGame : ModuleRules
{
	public MyShooterCPPGame(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(new string[] { 
            "Core", 
            "CoreUObject", 
            "Engine", 
            "InputCore" , 
            "Niagara" ,
            "PhysicsCore",
            "GameplayTasks",
            "NavigationSystem"});

		PrivateDependencyModuleNames.AddRange(new string[] {  });

        PublicIncludePaths.AddRange(new string[] { 
            "MyShooterCPPGame/Public/Player", 
            "MyShooterCPPGame/Public/Components",
            "MyShooterCPPGame/Public/Weapon",
            "MyShooterCPPGame/Public/UI" ,
            "MyShooterCPPGame/Public/PickUP",
            "MyShooterCPPGame/Public/AI",
            "MyShooterCPPGame/Public/Menu"});

        // Uncomment if you are using Slate UI
        // PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });

        // Uncomment if you are using online features
        // PrivateDependencyModuleNames.Add("OnlineSubsystem");

        // To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
    }
}
