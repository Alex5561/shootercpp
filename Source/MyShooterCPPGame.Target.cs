// My Shoot Game C++

using UnrealBuildTool;
using System.Collections.Generic;

public class MyShooterCPPGameTarget : TargetRules
{
	public MyShooterCPPGameTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "MyShooterCPPGame" } );
	}
}
